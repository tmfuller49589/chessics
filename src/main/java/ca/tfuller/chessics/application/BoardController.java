package ca.tfuller.chessics.application;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.chessics.bitboard.BoardOps;
import ca.tfuller.chessics.bitboard.ColorEnum;
import ca.tfuller.chessics.bitboard.FEN;
import ca.tfuller.chessics.bitboard.Game;
import ca.tfuller.chessics.bitboard.MoveAnalyser;
import ca.tfuller.chessics.bitboard.PieceEnum;
import ca.tfuller.chessics.bitboard.StateOps;
import ca.tfuller.chessics.bitboard.Utils;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Duration;

public class BoardController {
	private static final Logger logger = LogManager.getLogger(BoardController.class);
	// Reference to the main application.
	private ChessicsApp main;

	private Game game;

	private Image greenSpotImage = new Image(
			BoardController.class.getClassLoader().getResourceAsStream("greenSpot.png"));
	private Image redSpotImage = new Image(BoardController.class.getClassLoader().getResourceAsStream("redSpot.png"));

	private StackPane[][] stackPanes = new StackPane[8][8];
	private Square moveFromSquare = null;
	private Set<Square> legalSquares = new HashSet<Square>();
	private Set<Square> attackerSquares = new HashSet<Square>();

	private ArrayList<PieceEnum> capturedPieces = new ArrayList<PieceEnum>();
	private Square[][] squares = new Square[8][8];

	private MoveAnalyser moveAnalyser = null;

	@FXML
	private FlowPane capturedBlackPane;

	@FXML
	private FlowPane capturedWhitePane;

	@FXML
	private TextArea moveListTextArea;

	@FXML
	private GridPane boardGridPane;

	@FXML
	private StackPane boardStackPane;

	/**
	 * Initializes the controller class. This method is automatically called after
	 * the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		logger.info("initialize");

	}

	public void initBoard() {
		game = new Game();
		game.newGame();

		setBoardSquareColors();
		setupPieces();

		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				StackPane stackPane = new StackPane();
				stackPanes[row][col] = stackPane;
				boardGridPane.add(stackPane, col + 1, 7 - row + 1);

				Square square = squares[row][col];

				if (square.getColor().equals(ColorEnum.WHITE)) {
					stackPane.setStyle("-fx-background-color: #ffffff; ");
				} else {
					stackPane.setStyle("-fx-background-color: #999999; ");
				}

				Button button = new Button();
				button.setUserData(square);
				button.setOnMouseClicked((event) -> squareClicked(event));
				button.setOpacity(0);
				button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
				stackPane.getChildren().add(button);
			}
		}

		for (int row = 0; row <= 7; ++row) {
			Label l = new Label(Integer.toString(7 - row + 1));
			StackPane stackPane = new StackPane();
			stackPane.getChildren().add(l);
			boardGridPane.add(stackPane, 0, row + 1);
		}

		for (int col = 0; col <= 7; ++col) {
			char c = (char) ('a' + col);
			Label l = new Label(Character.toString(c));
			StackPane stackPane = new StackPane();
			stackPane.getChildren().add(l);
			boardGridPane.add(stackPane, col + 1, 0);
		}

		moveAnalyser = new MoveAnalyser();
	}

	/*
	 * Clears the displayed board.
	 * Does not clear the underlying board position.
	 */
	public void clearScreenBoard() {
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				StackPane stackPane = stackPanes[row][col];

				Iterator<Node> nodeIt = stackPane.getChildren().iterator();
				while (nodeIt.hasNext()) {
					Node node = nodeIt.next();
					if (node.getClass() == Label.class) {
						nodeIt.remove();
					}
				}
			}
		}
	}

	public void displayPieces() {
		clearScreenBoard();
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				StackPane stackPane = stackPanes[row][col];

				Label l = new Label();
				Square square = squares[row][col];
				l.setMouseTransparent(true);
				if (square.getPiece() != null) {
					l.setGraphic(new ImageView(PieceImages.getImage(square.getPiece())));
					l.toFront();
				}

				stackPane.getChildren().add(l);
			}
		}
	}

	private void clearSpots() {
		clearSpots(legalSquares);
		clearSpots(attackerSquares);
	}

	private void clearSpots(Set<Square> spotSquares) {
		for (Square spotSquare : spotSquares) {
			Iterator<Node> nodeIt = stackPanes[spotSquare.getRow()][spotSquare.getCol()].getChildren().iterator();
			while (nodeIt.hasNext()) {
				Node node = nodeIt.next();
				if (node.getClass().equals(Label.class)) {
					Label l = (Label) node;
					if (l.getUserData() != null) {
						if (l.getUserData().equals("dot")) {
							nodeIt.remove();
						}
					}
				}
			}
		}

	}

	private void squareClicked(MouseEvent event) {
		// remove any previous indicated legal moves
		clearSpots();

		moveAnalyser.setBitboards(game.getLastBitboards(), BoardOps.getSquares(game.getLastBitboards()));
		logger.info("xxxxxxxxxxxxxxxxxxxx current color is " + BoardOps.getColor(game.getLastBitboards()));
		logger.info("Fen: " + FEN.toString(game.getLastBitboards()));

		Button button = (Button) event.getSource();
		Square square = (Square) button.getUserData();
		logger.info(BoardOps.getColor(game.getLastBitboards()) + " to move.");
		logger.info("clicked " + Utils.getCoordsString(square.getRow(), square.getCol()));
		if (square.getPiece() != null) {
			logger.info(
					Utils.getCoordsString(square.getRow(), square.getCol()) + " has a " + square.getPiece().toString());
		} else {
			logger.info(Utils.getCoordsString(square.getRow(), square.getCol()) + " is empty");

		}
		logger.info("moveFromSquare = " + moveFromSquare);

		if (event.getButton() == MouseButton.SECONDARY) {
			moveFromSquare = null;

			// display attackers of the square clicked
			displayAttackers(square);
		} else if (event.getButton() == MouseButton.PRIMARY) {
			if (moveFromSquare == null) {
				selectPiece(square);
			} else {
				selectDestination(square);
			}
		}
	}

	private void movePiece(Square square) {
		// check for pawn promotion
		PieceEnum piece = moveFromSquare.getPiece();
		int promotionRow = piece.getColor().equals(ColorEnum.BLACK) ? 0 : 7;
		PieceEnum pawnPromotion = null;
		if ((piece.equals(PieceEnum.WPAWN) || piece.equals(PieceEnum.BPAWN)) && square.getRow() == promotionRow) {
			piece = getPawnPromotionChoice(piece.getColor());
			pawnPromotion = piece;
		}
		Square origSource = new Square(moveFromSquare);
		Square origDest = new Square(square);

		int moveFromIdx = Utils.getIndex(moveFromSquare.getRow(), moveFromSquare.getCol());
		int moveToIdx = Utils.getIndex(square.getRow(), square.getCol());

		game.movePiece(moveFromIdx, moveToIdx, pawnPromotion);
		updateSquares();
		displayPieces();
		animateMove(origSource, origDest);
		updateAfterMove();

		moveFromSquare = null;
		legalSquares.clear();

		moveAnalyser.setBitboards(game.getLastBitboards(), BoardOps.getSquares(game.getLastBitboards()));

		boolean check = moveAnalyser.isKingInCheck();
		logger.info("King is in check: " + check);
		if (check) {
			logger.info("color is " + BoardOps.getColor(game.getLastBitboards()));
			int kingIdx = moveAnalyser.getKingIndex(BoardOps.getColor(game.getLastBitboards()));
			long attackers = moveAnalyser.getKingAttackedBy(kingIdx, BoardOps.getColor(game.getLastBitboards()));
			Utils.printBoard(attackers);
		}

		boolean checkMate = moveAnalyser.checkForMate();

		if (checkMate) {
			Alert alert = new Alert(AlertType.INFORMATION, BoardOps.getColor(game.getLastBitboards()) + " is mated.",
					ButtonType.OK);
			alert.showAndWait();

			logger.info(BoardOps.getColor(game.getLastBitboards()) + " is mated.");

		}
	}

	private void selectDestination(Square square) {
		if (legalSquares.contains(square)) {
			// move the piece
			movePiece(square);
			return;
		}

		// user has already selected a piece but
		// user clicked on a square not marked as a legal square for a destination
		if (square.getPiece() == null || square.getPiece().equals(PieceEnum.EMPTY)) {
			moveFromSquare = null;
			// user clicked on an empty square
			return;
		}

		// user has already selected a piece but
		// user clicked on a square not marked as a legal square for a destination
		// user has clicked on a piece
		PieceEnum piece = square.getPiece();
		if (piece.getColor().equals(BoardOps.getColor(game.getLastBitboards()).getOppositeColor())) {
			logger.info("clicked on opponent's piece");
			moveFromSquare = null;
			legalSquares.clear();
			return;
		}

		// user has already selected a piece but
		// user clicked on a square not marked as a legal square for a destination
		// user has clicked on their own piece
		// switch the selection of legal squares
		moveFromSquare = square;
		displayLegalSquares(square);

	}

	private void selectPiece(Square square) {
		if (square.getPiece() == null || square.getPiece().equals(PieceEnum.EMPTY)) {
			moveFromSquare = null;
			legalSquares.clear();
			return;
		}
		PieceEnum piece = square.getPiece();
		if (piece.getColor().equals(BoardOps.getColor(game.getLastBitboards()).getOppositeColor())) {
			logger.info("clicked on opponent's piece");
			moveFromSquare = null;
			legalSquares.clear();
			return;
		}

		moveFromSquare = square;
		displayLegalSquares(square);
	}

	private void updateSquares() {
		byte[] bbsquares = BoardOps.getSquares(game.getLastBitboards());
		for (int row = 0; row <= 7; row++) {
			for (int col = 0; col <= 7; ++col) {
				int idx = Utils.getIndex(row, col);
				squares[row][col].setPiece(PieceEnum.fromEncoding(bbsquares[idx]));
			}
		}
	}

	private void displayAttackers(Square square) {
		long attackers = moveAnalyser.findAttackers(square.getIndex());
		attackerSquares = convertToSquares(attackers);
		for (Square sq : attackerSquares) {
			Label dotLabel = new Label();
			dotLabel.setUserData("dot");
			dotLabel.setGraphic(new ImageView(redSpotImage));
			dotLabel.setMouseTransparent(true);
			stackPanes[sq.getRow()][sq.getCol()].getChildren().add(dotLabel);
		}
	}

	private void displayLegalSquares(Square square) {

		if (square.getPiece() != null) {
			if (!square.getPiece().getColor().equals(BoardOps.getColor(game.getLastBitboards()))) {
				return;
			}
			int squareIndex = square.getIndex();
			int[] indexes = moveAnalyser.findLegalSquares(squareIndex);
			legalSquares = convertToSquares(indexes);
			moveFromSquare = square;
			logger.info(square.getPiece().getColor() + " " + square.getPiece());
		} else {
			logger.info(square.getCoordsString() + " empty square ");
			// moveAnalyser.findAttackers(square);
			return;
		}

		logger.info("number of legal moves is " + legalSquares.size());
		for (Square legalSquare : legalSquares) {
			Label dotLabel = new Label();
			dotLabel.setUserData("dot");
			dotLabel.setGraphic(new ImageView(greenSpotImage));
			dotLabel.setMouseTransparent(true);
			stackPanes[legalSquare.getRow()][legalSquare.getCol()].getChildren().add(dotLabel);
		}
	}

	private Set<Square> convertToSquares(int[] indexes) {
		Set<Square> squareSet = new HashSet<Square>();

		for (int index : indexes) {
			int row = Utils.getRowIdx(index);
			int col = Utils.getColIdx(index);
			squareSet.add(squares[row][col]);
		}
		return squareSet;
	}

	/*
	 * Given a bitboard, return an aray of Squares for
	 * the bits that are set in bitboard.
	 */
	private Set<Square> convertToSquares(long bitboard) {
		Set<Square> squareSet = new HashSet<Square>();

		for (int i = 0; i < 64; ++i) {
			if ((bitboard & 1) == 1) {
				int row = Utils.getRowIdx(i);
				int col = Utils.getColIdx(i);
				squareSet.add(squares[row][col]);
			}
			bitboard = bitboard >>> 1l;
		}
		return squareSet;
	}

	private void updateAfterMove() {
		displayMoveList();
		displayCapturedPieces();
	}

	private void animateMove(Square source, Square destination) {

		logger.info("board stack pane is " + boardStackPane);

		ImageView iv = new ImageView();
		iv.setImage(PieceImages.getImage(source.getPiece()));
		Label l = new Label();
		// l.setStyle("-fx-background-color: #0000bb");
		l.setMouseTransparent(true);
		l.setGraphic(iv);
		l.toFront();

		Group group = new Group(l);
		group.setUserData("animate");

		StackPane spSource = stackPanes[source.getRow()][source.getCol()];
		StackPane spDest = stackPanes[destination.getRow()][destination.getCol()];

		boardGridPane.add(group, source.getCol() + 1, 7 - source.getRow() + 1);
		double squareWidth = spSource.getWidth();
		double squareHeight = spSource.getHeight();
		double labelWidth = 0;
		double labelHeight = 0;

		for (Node child : spSource.getChildren()) {
			if (child instanceof Label) {
				labelWidth = ((Label) child).getWidth();
				labelHeight = ((Label) child).getHeight();
			}
		}
		double offsetX = (squareWidth - labelWidth) / 2d;
		double offsetY = (squareHeight - labelHeight) / 2d;
		group.setTranslateX(offsetX);
		// group.setTranslateY(offsetY);

		double x1 = spSource.getLayoutX();
		double y1 = spSource.getLayoutY();
		double x2 = spDest.getLayoutX();
		double y2 = spDest.getLayoutY();

		logger.info("translation animation from " + " " + x1 + "," + y1 + " to  " + x2 + "," + y2);

		// Creating Translate Transition
		TranslateTransition translateTransition = new TranslateTransition();

		// Setting the duration of the transition
		translateTransition.setDuration(Duration.millis(500));

		// Setting the value of the transition along the x axis.
		translateTransition.setByX(x2 - x1);
		translateTransition.setByY(y2 - y1);

		translateTransition.setCycleCount(1);
		translateTransition.setAutoReverse(false);

		translateTransition.setOnFinished(e -> animateFinished());

		// Setting the node for the transition
		translateTransition.setNode(group);

		// Playing the animation
		translateTransition.play();

		// main.getRootLayout().getChildren().add(group);

	}

	private Object animateFinished() {
		Iterator<Node> nodeIt = boardGridPane.getChildren().iterator();
		while (nodeIt.hasNext()) {
			Node node = nodeIt.next();
			if (node.getUserData() != null && node.getUserData().equals("animate")) {
				nodeIt.remove();
			}
		}
		return null;
	}

	private PieceEnum getPawnPromotionChoice(ColorEnum color) {
		List<String> choices = new ArrayList<>();
		if (StateOps.getColor(game.getLastBitboards()).equals(ColorEnum.WHITE)) {
			choices.add("Queen");
			choices.add("Night");
			choices.add("Bishop");
			choices.add("Rook");
		} else {
			choices.add("queen");
			choices.add("night");
			choices.add("bishop");
			choices.add("rook");
		}

		ChoiceDialog<String> dialog = new ChoiceDialog<>("queen", choices);
		dialog.setTitle("Pawn promotion");
		dialog.setHeaderText("Choose promotion piece");

		Optional<String> result = dialog.showAndWait();
		PieceEnum piece = null;
		if (result.isPresent()) {
			logger.info("Your choice: " + result.get());
			piece = PieceEnum.fromChar(result.get().charAt(0));
		} else {
			piece = PieceEnum.fromCharColor('q', StateOps.getColor(game.getLastBitboards()));
		}
		return piece;
	}

	private void displayMoveList() {
		String moves = "";

		logger.info("fenlist size is " + game.getFenList().size());

		for (int i = 0; i < game.getFenList().size() - 1; ++i) {

			if (i % 2 == 1) {
				moves += " ";
			} else {
				moves += (i / 2) + 1 + ". ";
			}
			moves += BoardOps.getMove(game.getGameBitboards().get(i), game.getGameBitboards().get(i + 1));

			if (i % 2 == 1) {
				moves += System.lineSeparator();
			}
		}

		moveListTextArea.setText(moves);
		// causes text area to scroll to bottom;
		moveListTextArea.appendText("");
	}

	private void displayCapturedPieces() {
		capturedBlackPane.getChildren().clear();
		capturedWhitePane.getChildren().clear();
		for (PieceEnum piece : capturedPieces) {
			String unicode = UnicodeChessMap.get(piece);
			Text text = new Text(unicode);
			text.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 30));
			if (piece.getColor().equals(ColorEnum.BLACK)) {
				capturedBlackPane.getChildren().add(text);
			} else {
				capturedWhitePane.getChildren().add(text);
			}
		}
	}

	private void removePieces(Square square) {
		StackPane sp = stackPanes[square.getRow()][square.getCol()];
		Iterator<Node> nodeIt = sp.getChildren().iterator();

		while (nodeIt.hasNext()) {
			Node node = nodeIt.next();
			if (node.getClass().equals(Label.class)) {
				nodeIt.remove();
			}
		}
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */
	public void setMainApp(ChessicsApp main) {
		this.main = main;

	}

	@FXML
	private void printBoardClicked() {
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				PieceEnum piece = squares[row][col].getPiece();
				if (piece != null) {
					logger.info("piece at " + Utils.getCoordsString(row, col) + " is " + piece.toString());
				} else {
					logger.info("square " + Utils.getCoordsString(row, col) + " is empty.");
				}
			}
		}
	}

	@FXML
	private void saveGameClicked() {
		FileChooser fc = new FileChooser();
		File file = fc.showSaveDialog(null);
		if (file == null) {
			return;
		}

		String text = "";
		for (String s : game.getFenList()) {
			text += s + System.lineSeparator();
		}

		try {
			Files.write(file.toPath(), text.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadGame(File file) {
		try {

			game.loadGame(file);
			getCapturedPieces();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void loadGameClicked() {
		Alert alert = new Alert(AlertType.CONFIRMATION, "Discard current game and load a saved game?", ButtonType.YES,
				ButtonType.NO);
		alert.showAndWait();

		if (alert.getResult() != ButtonType.YES) {
			return;
		}

		FileChooser fc = new FileChooser();
		File file = fc.showOpenDialog(null);
		if (file == null) {
			return;
		}

		newGame();
		clearScreenBoard();
		loadGame(file);

		logger.info("current player color is " + BoardOps.getColor(game.getLastBitboards()));

		displayPieces();
		displayMoveList();
		displayCapturedPieces();

	}

	@FXML
	public void newGameClicked() {
		Alert alert = new Alert(AlertType.CONFIRMATION, "Discard current game and begin a new one?", ButtonType.YES,
				ButtonType.NO);
		alert.showAndWait();

		if (alert.getResult() != ButtonType.YES) {
			return;
		}

		newGame();
	}

	public void newGame() {
		logger.info("new game");
		initBoard();
		capturedPieces.clear();

		StateOps.setColor(game.getLastBitboards(), ColorEnum.WHITE);

		capturedBlackPane.getChildren().clear();
		capturedWhitePane.getChildren().clear();
		moveFromSquare = null;
		legalSquares.clear();
		moveListTextArea.setText("");

		displayPieces();
	}

	@FXML
	private void takeBack() {
		logger.info("take back move");
		clearScreenBoard();

		game.takeBack();

		updateSquares();
		displayPieces();
		updateAfterMove();
	}

	private void setBoardSquareColors() {
		ColorEnum color = ColorEnum.BLACK;
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				Square square = new Square(row, col, null, color);
				squares[row][col] = square;
				color = ColorEnum.getOppositeColor(color);
			}
			color = ColorEnum.getOppositeColor(color);
		}
	}

	private void setupPieces() {
		// Black pawns
		for (int col = 0; col <= 7; ++col) {
			squares[7 - 1][col].setPiece(PieceEnum.BPAWN);
		}
		squares[7][0].setPiece(PieceEnum.BROOK);
		squares[7][7].setPiece(PieceEnum.BROOK);
		squares[7][1].setPiece(PieceEnum.BNIGHT);
		squares[7][6].setPiece(PieceEnum.BNIGHT);
		squares[7][2].setPiece(PieceEnum.BBISHOP);
		squares[7][5].setPiece(PieceEnum.BBISHOP);
		squares[7][4].setPiece(PieceEnum.BKING);
		squares[7][3].setPiece(PieceEnum.BQUEEN);
		// White pawns

		for (int col = 0; col <= 7; ++col) {
			squares[0 + 1][col].setPiece(PieceEnum.WPAWN);
		}
		squares[0][0].setPiece(PieceEnum.WROOK);
		squares[0][7].setPiece(PieceEnum.WROOK);
		squares[0][1].setPiece(PieceEnum.WNIGHT);
		squares[0][6].setPiece(PieceEnum.WNIGHT);
		squares[0][2].setPiece(PieceEnum.WBISHOP);
		squares[0][5].setPiece(PieceEnum.WBISHOP);
		squares[0][4].setPiece(PieceEnum.WKING);
		squares[0][3].setPiece(PieceEnum.WQUEEN);
	}

	public void getCapturedPieces() {

		capturedPieces = new ArrayList<PieceEnum>();

		EnumMap<PieceEnum, Set<Square>> presentPieces = findPresentPieces();

		EnumMap<PieceEnum, Integer> numberOfPieces = new EnumMap<PieceEnum, Integer>(PieceEnum.class);
		numberOfPieces.put(PieceEnum.WPAWN, 8);
		numberOfPieces.put(PieceEnum.WNIGHT, 2);
		numberOfPieces.put(PieceEnum.WBISHOP, 2);
		numberOfPieces.put(PieceEnum.WROOK, 2);
		numberOfPieces.put(PieceEnum.WQUEEN, 1);
		numberOfPieces.put(PieceEnum.WKING, 1);
		numberOfPieces.put(PieceEnum.BPAWN, 8);
		numberOfPieces.put(PieceEnum.BNIGHT, 2);
		numberOfPieces.put(PieceEnum.BBISHOP, 2);
		numberOfPieces.put(PieceEnum.BROOK, 2);
		numberOfPieces.put(PieceEnum.BQUEEN, 1);
		numberOfPieces.put(PieceEnum.BKING, 1);

		for (PieceEnum piece : PieceEnum.values()) {
			int fullCount = numberOfPieces.get(piece);
			int count = presentPieces.get(piece).size();
			for (int i = 0; i < fullCount - count; ++i) {
				capturedPieces.add(piece);
			}
		}
	}

	public EnumMap<PieceEnum, Set<Square>> findPresentPieces() {
		EnumMap<PieceEnum, Set<Square>> presentPieces = new EnumMap<PieceEnum, Set<Square>>(PieceEnum.class);

		for (PieceEnum pte : PieceEnum.values()) {
			Set<Square> squares1 = new HashSet<Square>();
			presentPieces.put(pte, squares1);
		}

		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				PieceEnum piece = squares[row][col].getPiece();
				if (piece != null) {
					Set<Square> sq = presentPieces.get(piece);
					sq.add(squares[row][col]);
				}
			}
		}

		return presentPieces;
	}
}
