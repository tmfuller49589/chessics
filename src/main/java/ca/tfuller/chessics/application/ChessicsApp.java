package ca.tfuller.chessics.application;

import java.awt.Desktop;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ChessicsApp extends Application {
	private Stage primaryStage;
	private AnchorPane rootLayout;
	private Scene scene;
	private static final Logger logger = LogManager.getLogger();

	@Override
	public void start(Stage primaryStage) {
		try {
			this.primaryStage = primaryStage;
			this.primaryStage.setTitle("Chessics");

			initRootLayout();

		} catch (

		Exception e) {
			e.printStackTrace();
		}
	}

	public AnchorPane getRootLayout() {
		return rootLayout;
	}

	public static void main(String[] args) {
		launch(args);

		Desktop desktop = null;
		if (Desktop.isDesktopSupported()) {
			desktop = Desktop.getDesktop();
		}

	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {

			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(ChessicsApp.class.getResource("/ca/tfuller/chessics/application/BoardView.fxml"));

			rootLayout = (AnchorPane) loader.load();

			// Show the scene containing the root layout.
			scene = new Scene(rootLayout);

			BoardController controller = loader.getController();
			controller.setMainApp(this);
			controller.initBoard();
			controller.displayPieces();

			scene.getRoot().requestFocus();

			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Scene getScene() {
		return scene;
	}
}
