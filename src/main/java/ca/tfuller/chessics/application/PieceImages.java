package ca.tfuller.chessics.application;

import java.util.EnumMap;

import ca.tfuller.chessics.bitboard.PieceEnum;
import javafx.scene.image.Image;

class PieceImages {
	private static EnumMap<PieceEnum, Image> pieceImages = new EnumMap<PieceEnum, Image>(PieceEnum.class);

	static {
		for (PieceEnum piece : PieceEnum.values()) {
			if (piece != PieceEnum.EMPTY) {
				String imageName = piece.toString() + ".png";
				imageName = imageName.toLowerCase();
				Image image = new Image(PieceImages.class.getClassLoader().getResourceAsStream(imageName));
				pieceImages.put(piece, image);
			}
		}
	}

	public static Image getImage(PieceEnum piece) {
		Image image = pieceImages.get(piece);
		return image;
	}
}
