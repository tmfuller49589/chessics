package ca.tfuller.chessics.application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.chessics.bitboard.ColorEnum;
import ca.tfuller.chessics.bitboard.PieceEnum;
import ca.tfuller.chessics.bitboard.Utils;

class Square {
	private PieceEnum piece = null;
	private ColorEnum color;
	private int row;
	private int col;
	private static final Logger logger = LogManager.getLogger(Square.class);

	public Square(int row, int col, PieceEnum piece, ColorEnum color) {
		this.row = row;
		this.col = col;
		this.piece = piece;
		this.color = color;
	}

	public Square(Square s) {
		this.row = s.row;
		this.col = s.col;
		this.piece = s.piece;
		this.color = s.color;
	}

	public Square() {

	}

	public int getIndex() {
		return Utils.getIndex(row, col);
	}

	public Square copy() {
		Square s = new Square();
		s.row = row;
		s.col = col;
		s.piece = piece;
		s.color = color;
		return s;
	}

	public PieceEnum getPiece() {
		return piece;
	}

	public void setPiece(PieceEnum piece) {
		this.piece = piece;
	}

	public ColorEnum getColor() {
		return color;
	}

	public void setColor(ColorEnum color) {
		this.color = color;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public String getCoordsString() {
		return Utils.getCoordsString(row, col);
	}

	public void printCoords() {
		logger.info(getCoordsString());
	}

	@Override
	public String toString() {
		String s = getCoordsString();
		if (piece != null) {
			s += " " + piece.toString();
		}
		return s;
	}
}
