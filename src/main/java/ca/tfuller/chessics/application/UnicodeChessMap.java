package ca.tfuller.chessics.application;

import java.util.HashMap;
import java.util.Map;

import ca.tfuller.chessics.bitboard.PieceEnum;

public class UnicodeChessMap {

	private static Map<PieceEnum, String> unicodeMap = new HashMap<PieceEnum, String>();
	//@formatter:off
	static {
		 unicodeMap.put(PieceEnum.WPAWN,   "\u2659");
		 unicodeMap.put(PieceEnum.WNIGHT,  "\u2658");
		 unicodeMap.put(PieceEnum.WBISHOP, "\u2657");
		 unicodeMap.put(PieceEnum.WROOK,   "\u2656");
		 unicodeMap.put(PieceEnum.WQUEEN,  "\u2655");
		 unicodeMap.put(PieceEnum.WKING,   "\u2654");
		 unicodeMap.put(PieceEnum.BPAWN,   "\u265F");
		 unicodeMap.put(PieceEnum.BNIGHT,  "\u265E");
		 unicodeMap.put(PieceEnum.BBISHOP, "\u265D");
		 unicodeMap.put(PieceEnum.BROOK,   "\u265C");
		 unicodeMap.put(PieceEnum.BQUEEN,  "\u265B");
		 unicodeMap.put(PieceEnum.BKING,   "\u265A");
	}
	//@formatter:on

	public static String get(PieceEnum piece) {
		return unicodeMap.get(piece);
	}
}
