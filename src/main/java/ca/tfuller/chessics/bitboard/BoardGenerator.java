package ca.tfuller.chessics.bitboard;

import java.util.concurrent.ThreadLocalRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BoardGenerator {

	private static final Logger logger = LogManager.getLogger(BoardGenerator.class);

	private static ThreadLocalRandom rgen;

	private static long notAFile = 0xfefefefefefefefel; // ~0x0101010101010101
	private static long notHFile = 0x7f7f7f7f7f7f7f7fl; // ~0x8080808080808080

	// one bitboard for a knight's 8 moves
	private static final long knight = 0b0000000000000000000000000000101000010001000000000001000100001010L;

	// one bitboard for a king's 8 moves
	private static final long king = 0b0000000000000000000000000000000000000000000001110000010100000111L;

	private static final long diag = 0b1000000001000000001000000001000000001000000001000000001000000001L;
	private static final long adiag = 0b0000000100000010000001000000100000010000001000000100000010000000L;
	private static final long perimeter = 0b1111111110000001100000011000000110000001100000011000000111111111L;

	// two (white & black) by 64 (squares)
	private static final long pawnAttacksMask[][] = new long[2][];

	private static final long knightMovesMask[] = new long[64];
	private static final long bishopMovesMask[] = new long[64];
	private static final long kingMovesMask[] = new long[64];
	private static final long rookMovesMask[] = new long[64];
	private static final long queenMovesMask[] = new long[64];

	private static final long bishopMoves[][] = new long[64][];
	private static final long rookMoves[][] = new long[64][];

	private static final int rookBitShift[] = new int[64];
	private static final int bishopBitShift[] = new int[64];
	private static final long rookMagic[] = new long[64];
	private static final long rookBlocker[][] = new long[64][];
	private static final long bishopMagic[] = new long[64];
	private static final long bishopBlocker[][] = new long[64][];

	private static final long rows[] = new long[8];
	private static final long cols[] = new long[8];

	public static long squares[] = new long[64];

	static int maxBits = 44;

	static long counter = 0;
	static {

		generateSquares();
		generateColBoards();
		generateRowBoards();

		generatePawnMoves();
		generatePawnAttacks();
		generateKnightMoves();
		generateBishopMasks();
		generateRookMasks();
		generateQueenMoves();
		generateKingMoves();

		generatePawnAttacks();

		logger.info("generating rook blocker permutations");
		generateBlockerPermutations(rookMovesMask, rookBlocker, rookMoves, PieceEnum.WROOK);
		logger.info("generating bishop blocker permutations");
		generateBlockerPermutations(bishopMovesMask, bishopBlocker, bishopMoves, PieceEnum.WBISHOP);

		logger.info("generating rook magic numbers");
		generateMagic(rookMoves, rookBlocker, rookBitShift, rookMagic, rookMovesMask);
		logger.info("generating bishop magic numbers");
		generateMagic(bishopMoves, bishopBlocker, bishopBitShift, bishopMagic, bishopMovesMask);

		printBishopMagics();
		printRookMagics();
	}

	public static void main(String[] args) {

		logger.info("finished");

	}

	private static void printBishopBlockerPermutations() {

		for (int idx = 0; idx < 64; ++idx) {
			for (int i = 0; i < Math.pow(2, bishopBitShift[idx]); ++i) {
				logger.info("idx = " + idx + " i = " + i);
				Utils.printBoard(bishopBlocker[idx][i]);
			}
		}
	}

	static long randLongx() {
		return rgen.nextLong();
	}

	static long random_uint64() {
		long u1, u2, u3, u4, u5, u6, u7, u8;
		u1 = (long) (ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE)) & 0xFFFF;
		u2 = (long) (ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE)) & 0xFFFF;
		u3 = (long) (ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE)) & 0xFFFF;
		u4 = (long) (ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE)) & 0xFFFF;

		u5 = u2 << 16;
		u6 = u3 << 32;
		u7 = u4 << 48;

		u8 = u1 | u5 | u6 | u7;

		return u8;
	}

	static long random_uint64_fewbits() {
		return random_uint64() & random_uint64() & random_uint64();
	}

	static long randLong() {
		int maxZeroBits = maxBits;
		// int zeroBits = rgen.nextInt(maxZeroBits + 1);
		long r = 0x0l;
		for (int i = 0; i < 10; ++i) {
			// int idx = rgen.nextInt(64);
			int idx = ThreadLocalRandom.current().nextInt(64);
			r = Utils.setBit(r, idx);
		}
		r = Utils.clearBit(r, 63);
		return r;
	}

	static long randLongXXXX() {
		int maxZeroBits = maxBits;
		int zeroBits = rgen.nextInt(maxZeroBits + 1);
		long r = 0x0l;
		for (int i = 0; i < zeroBits; ++i) {
			int idx = rgen.nextInt(64);
			r = Utils.setBit(r, idx);
		}
		r = Utils.clearBit(r, 63);
		return r;
	}

	static long randLongXX() {
		int maxZeroBits = 40;
		int zeroBits = rgen.nextInt(maxZeroBits + 1);
		long r = 0xFFFFFFFFFFFFFFFFl;
		for (int i = 0; i < zeroBits; ++i) {
			int idx = rgen.nextInt(64);
			r = Utils.clearBit(r, idx);
		}
		r = Utils.clearBit(r, 63);
		return r;
	}

	// from https://www.chessprogramming.org/Looking_for_Magics
	// generating rook magics for this java program
	// using computeMagicIndex32 3.8 \pm 1 second
	// using computeMagicIndex64 10.3 \pm 2 second
	// C program:from https://www.chessprogramming.org/Looking_for_Magics
	// using 64 bit multiply: 12 second
	// using 32 bit multiply: 2.4 second
	private static int computeMagicIndex32(long b, long magic, int bits) {

		// (unsigned)((int)b*(int)magic ^ (int)(b>>32)*(int)(magic>>32)) >> (32-bits);

		int a = (int) b * (int) magic;
		int c = (int) (b >>> 32) * (int) (magic >>> 32);
		int d = a ^ c;
		int e = d >>> 32 - bits;
		return e;

	}

	public static int computeMagicIndex(long blockerboard, long magic, int bits) {
		return computeMagicIndex32(blockerboard, magic, bits);
	}

	private static int computeMagicIndex64(long blockerboard, long magic, int bits) {
		long bm = blockerboard * magic;
		long bms = bm >>> (64 - bits);
		return (int) bms;
	}

	static long findFirstBlockers(long blockers, long piecePosition, PieceEnum piece) {
		long firstBlockers = 0;
		switch (piece) {
		case WROOK:
		case BROOK:
			firstBlockers = findFirstBlocker(blockers, piecePosition, DirectionEnum.N);
			firstBlockers |= findFirstBlocker(blockers, piecePosition, DirectionEnum.E);
			firstBlockers |= findFirstBlocker(blockers, piecePosition, DirectionEnum.S);
			firstBlockers |= findFirstBlocker(blockers, piecePosition, DirectionEnum.W);
			break;
		case WBISHOP:
		case BBISHOP:
			firstBlockers = findFirstBlocker(blockers, piecePosition, DirectionEnum.NE);
			firstBlockers |= findFirstBlocker(blockers, piecePosition, DirectionEnum.SE);
			firstBlockers |= findFirstBlocker(blockers, piecePosition, DirectionEnum.SW);
			firstBlockers |= findFirstBlocker(blockers, piecePosition, DirectionEnum.NW);
			break;
		default:
			logger.error("Error: shouldn't be here");
			System.exit(0);
			break;
		}

		return firstBlockers;

	}

	static long findFirstBlocker(long blockers, long piecePosition, DirectionEnum dir) {
		long x = piecePosition;
		long accumulator = x;

		for (int i = 0; i < 7; ++i) {

			switch (dir) {
			case N:
				x = x << Math.abs(dir.value);
				// x &= ~rows[0];
				break;
			case NE:
				x = x << Math.abs(dir.value);
				x &= ~rows[0];
				x &= ~cols[0];
				break;
			case E:
				x = x << Math.abs(dir.value);
				x &= ~cols[0];
				break;
			case SE:
				x = x >>> Math.abs(dir.value);
				x &= ~cols[0];
				x &= ~rows[7];
				break;
			case S:
				x = x >>> Math.abs(dir.value);
				// x &= ~rows[7];
				break;
			case SW:
				x = x >>> Math.abs(dir.value);
				x &= ~cols[7];
				x &= ~rows[7];
				break;
			case W:
				x = x >>> Math.abs(dir.value);
				x &= ~cols[7];
				break;
			case NW:
				x = x << Math.abs(dir.value);
				x &= ~cols[7];
				x &= ~rows[0];
				break;
			}
			accumulator |= x;
			if (blockers == (blockers | x)) {
				accumulator ^= piecePosition;
				return accumulator;
			}
		}
		accumulator ^= piecePosition;
		return accumulator;
	}

	private static void generateBlockerPermutations(long[] maskOcupancies, long[][] blockerBoard, long[][] moveBoard,
			PieceEnum piece) {

		// loop over all board positions in the occupancies array
		for (int idx = 0; idx < 64; ++idx) {

			long occupancy = maskOcupancies[idx];

			int bits = Long.bitCount(occupancy);
			blockerBoard[idx] = new long[(int) Math.pow(2, bits)];
			moveBoard[idx] = new long[(int) Math.pow(2, bits)];

			int[] bitIndex = new int[bits];

			int bii = 0;
			for (int i = 0; i < 64; ++i) {
				long bit = Utils.getBit(occupancy, i);
				if (bit == 1) {
					bitIndex[bii] = i;
					++bii;
				}
			}

			// loop over all possible arrangements of the
			// occupancy board - if there are 10 bits set
			// then there are 2^10 = 1024 different boards
			// with the occupancy bits set to either 0 or 1
			for (int i = 0; i < Math.pow(2, bits); ++i) {
				long xx = occupancy;
				for (int b = 0; b < bits; ++b) {
					int bit = Utils.getBit(i, b);
					xx = Utils.setBit(xx, bitIndex[b], bit);
				}

				blockerBoard[idx][i] = occupancy & xx;

				moveBoard[idx][i] = findFirstBlockers(blockerBoard[idx][i], squares[idx], piece);

				moveBoard[idx][i] = fixEdges(moveBoard[idx][i], blockerBoard[idx][i], squares[idx], piece);

				moveBoard[idx][i] ^= squares[idx];
			}
		}
	}

	static long fixEdges(long moveBoard, long blockerBoard, long pieceBoard, PieceEnum piece) {
		switch (piece) {
		case WROOK:
		case BROOK:
			return fixEdgesRook(moveBoard, blockerBoard, pieceBoard);
		case WBISHOP:
		case BBISHOP:
			return fixEdgesBishop(moveBoard, blockerBoard, pieceBoard);
		default:
			//
			logger.error("shouldn't be here");
			System.exit(0);
			return 0;
		}
	}

	static long fixEdges(long moveBoard, long blockerBoard, long pieceBoard, DirectionEnum dir) {
		long sum = 0;
		long previousPieceBoard = 0;
		long previousSum = 0;
		for (int i = 0; i <= 7; ++i) {
			previousPieceBoard = pieceBoard;
			// find first blockers: 8404992
			// logger.info("before shift:");
			// Utils.printBoard(pieceBoard);
			pieceBoard = shiftBoardOne(pieceBoard, dir);
			// logger.info("after shift:");
			// Utils.printBoard(pieceBoard);
			if (pieceBoard == 0) {
				// we have shifted piece off the board
				sum = previousSum;
				break;
			}
			previousSum = sum;
			sum += blockerBoard & pieceBoard;
		}
		if (sum == 0) {
			moveBoard |= previousPieceBoard;
		}
		return moveBoard;
	}

	static long fixEdgesRook(long moveBoard, long blockerBoard, long pieceBoard) {
		long fixedMoveBoard = moveBoard;
		DirectionEnum dir = DirectionEnum.N;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		dir = DirectionEnum.E;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		dir = DirectionEnum.S;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		dir = DirectionEnum.W;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		return fixedMoveBoard;
	}

	static long fixEdgesBishop(long moveBoard, long blockerBoard, long pieceBoard) {
		long fixedMoveBoard = moveBoard;
		DirectionEnum dir = DirectionEnum.NE;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		dir = DirectionEnum.SE;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		dir = DirectionEnum.SW;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		dir = DirectionEnum.NW;
		fixedMoveBoard |= fixEdges(fixedMoveBoard, blockerBoard, pieceBoard, dir);
		return fixedMoveBoard;
	}

	static void generatePawnMoves() {
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				int idx = Utils.getIndex(row, col);
				if (row >= 2) {

				}
			}
		}
	}

	static void generatePawnAttacks() {
		// generate white attacks

		for (ColorEnum color : ColorEnum.values()) {
			switch (color) {
			case WHITE:
				pawnAttacksMask[ColorEnum.WHITE.getValue()] = new long[64];
				// start at 8 because pawns are never on rank 0
				for (int i = 8; i < 64; ++i) {
					pawnAttacksMask[ColorEnum.WHITE.getValue()][i] = shiftBoardOne(squares[i], DirectionEnum.NE);
					pawnAttacksMask[ColorEnum.WHITE.getValue()][i] |= shiftBoardOne(squares[i], DirectionEnum.NW);
				}
				break;
			case BLACK:
				pawnAttacksMask[ColorEnum.BLACK.getValue()] = new long[64];
				// start at rank 7 because pawns are never on rank 8
				for (int i = 63 - 8; i >= 0; --i) {
					pawnAttacksMask[ColorEnum.BLACK.getValue()][i] = shiftBoardOne(squares[i], DirectionEnum.SE);
					pawnAttacksMask[ColorEnum.BLACK.getValue()][i] |= shiftBoardOne(squares[i], DirectionEnum.SW);
				}
				break;
			case NONE:
				continue;
			}
		}
	}

	private static void generateQueenMoves() {
		for (int i = 0; i < 64; ++i) {
			queenMovesMask[i] = rookMovesMask[i];
			queenMovesMask[i] |= bishopMovesMask[i];
		}
	}

	private static void generateBishopMasks() {
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				long move = 0;
				int idx = Utils.getIndex(row, col);

				int rowshift = 0;
				int colshift = 0;

				// northeast
				rowshift = row;
				colshift = col;
				long tmp = diag;
				tmp = shiftBoardByRow(tmp, rowshift);
				tmp = shiftBoardByColumn(tmp, colshift);
				move |= tmp;

				// southwest
				rowshift = row - 7;
				colshift = col - 7;
				tmp = diag;
				tmp = shiftBoardByRow(tmp, rowshift);
				tmp = shiftBoardByColumn(tmp, colshift);
				move |= tmp;

				// northwest
				rowshift = row;
				colshift = col - 7;
				tmp = adiag;
				tmp = shiftBoardByRow(tmp, rowshift);
				tmp = shiftBoardByColumn(tmp, colshift);
				move |= tmp;

				// southeast
				rowshift = row - 7;
				colshift = col;
				tmp = adiag;
				tmp = shiftBoardByRow(tmp, rowshift);
				tmp = shiftBoardByColumn(tmp, colshift);
				move |= tmp;

				move = Utils.clearBit(move, idx);
				move &= ~perimeter;
				bishopMovesMask[idx] = move;

				int bits = Long.bitCount(move);
				bishopBitShift[idx] = bits;
			}
		}
	}

	private static void printBishopMasks() {
		for (int idx = 0; idx < 64; ++idx) {
			logger.info("idx = " + idx);
			Utils.printBoard(bishopMovesMask[idx]);
		}
	}

	private static void printBishopMagics() {
		for (int idx = 0; idx < 64; ++idx) {
			logger.info("bishopMagic[" + idx + "] = " + bishopMagic[idx]);
		}
	}

	private static void printRookMagics() {
		for (int idx = 0; idx < 64; ++idx) {
			logger.info("rookMagic[" + idx + "] = " + rookMagic[idx]);
		}
	}

	private static void generateMagic(long[][] moves, long[][] blocker, int[] bitshift, long magicNumbers[],
			long[] mask) {
		// remember original moves array
		// This is necessary because the moves array is getting reindexed
		// according to magic index
		// Once we have a magic number found, set the move board using the magic index
		long[][] origMoves = new long[64][];

		for (int squareIdx = 0; squareIdx < 64; ++squareIdx) {
			origMoves[squareIdx] = new long[moves[squareIdx].length];
			System.arraycopy(moves[squareIdx], 0, origMoves[squareIdx], 0, moves[squareIdx].length);
		}

		// zero the moves array
		for (int squareIdx = 0; squareIdx < 64; ++squareIdx) {
			for (int i = 0; i < moves[squareIdx].length; ++i) {
				moves[squareIdx][i] = 0;
			}
		}

		for (int idx = 0; idx < 64; ++idx) {
			logger.info("generating magic numbers, square index " + idx);

			long magicNumber = 0;

			int magicIndex = -1;

			int twoPow = (int) Math.pow(2, bitshift[idx]);
			long[] movePositionUsed;
			magloop: do {
				movePositionUsed = new long[twoPow];
				magicNumber = random_uint64_fewbits();

				int[] magicIndexes = new int[twoPow];

				// compute magic indexes for this magic number for all permutations
				for (int comboIndex = 0; comboIndex < twoPow; ++comboIndex) {

					magicIndex = computeMagicIndex(blocker[idx][comboIndex], magicNumber, bitshift[idx]);
					if (Long.bitCount((mask[idx] * magicNumber) & 0xFF00000000000000l) < 6) {
						continue magloop;
					}

					// check for collision with already generated magic indexes
					if (movePositionUsed[magicIndex] == 0) {
						movePositionUsed[magicIndex] = origMoves[idx][comboIndex];
					} else if (movePositionUsed[magicIndex] != origMoves[idx][comboIndex]) {
						continue magloop;
					}

					magicIndexes[comboIndex] = magicIndex;
				}
				break;
			} while (true);

			// now that we have the magic index,
			// put the move that generated the magic index
			// into the magic index spot
			for (int i = 0; i < twoPow; ++i) {
				moves[idx][i] = movePositionUsed[i];
			}
			magicNumbers[idx] = magicNumber;
		}
	}

	private static void generateColBoards() {
		long col = 0b0000000100000001000000010000000100000001000000010000000100000001L;
		for (int shift = 0; shift < 8; ++shift) {
			cols[shift] = col;
			col = col << 1;
		}
	}

	private static void generateRowBoards() {
		long row = 0b0000000000000000000000000000000000000000000000000000000011111111L;
		for (int shift = 0; shift < 8; ++shift) {
			rows[shift] = row;
			row = row << 8;
		}
	}

	private static void generateRookMasks() {
		for (int row = 0; row <= 7; ++row) {
			for (int col = 0; col <= 7; ++col) {
				int idx = Utils.getIndex(row, col);
				long move = rows[row];
				move |= cols[col];

				// clear piece position
				move = Utils.clearBit(move, row, col);

				// clear perimeter
				move = Utils.clearBit(move, row, 7);
				move = Utils.clearBit(move, row, 0);
				move = Utils.clearBit(move, 0, col);
				move = Utils.clearBit(move, 7, col);
				rookMovesMask[idx] = move;
				int bits = Long.bitCount(move);

				rookBitShift[idx] = bits;

			}
		}
	}

	private static void generateKingMoves() {
		for (int row = 0; row <= 7; ++row) {
			int rowShift = -1 + row;
			for (int col = 0; col <= 7; ++col) {
				int colShift = -1 + col;
				int idx = Utils.getIndex(row, col);

				kingMovesMask[idx] = shiftBoardByColumn(king, colShift);
				kingMovesMask[idx] = shiftBoardByRow(kingMovesMask[idx], rowShift);
			}
		}
	}

	private static void generateKnightMoves() {
		for (int row = 0; row <= 7; ++row) {
			int rowShift = -2 + row;
			for (int col = 0; col <= 7; ++col) {
				int colShift = -2 + col;
				int idx = Utils.getIndex(row, col);

				knightMovesMask[idx] = shiftBoardByColumn(knight, colShift);
				knightMovesMask[idx] = shiftBoardByRow(knightMovesMask[idx], rowShift);
			}
		}
	}

	public static long getBishopAttacks(long occupied, int squareIndex) {
		long occ = occupied & BoardGenerator.getBishopmovesmask()[squareIndex];
		int magicIndex = BoardGenerator.computeMagicIndex(occ, BoardGenerator.getBishopmagic()[squareIndex],
				BoardGenerator.getBishopbitshift()[squareIndex]);
		return BoardGenerator.getBishopmoves()[squareIndex][magicIndex];
	}

	public static long getRookAttacks(long occupied, int squareIndex) {
		long occ = occupied & BoardGenerator.getRookmovesmask()[squareIndex];
		int magicIndex = BoardGenerator.computeMagicIndex(occ, BoardGenerator.getRookmagic()[squareIndex],
				BoardGenerator.getRookbitshift()[squareIndex]);
		return BoardGenerator.getRookmoves()[squareIndex][magicIndex];
	}

	static long shiftBoardOne(long board, DirectionEnum dir) {
		long shifted = board;
		switch (dir) {
		case N:
			shifted = board << Math.abs(dir.value);
			break;
		case NE:
			shifted = (board << Math.abs(dir.value)) & notAFile;
			break;
		case E:
			shifted = (board << Math.abs(dir.value)) & notAFile;
			break;
		case SE:
			shifted = (board >>> Math.abs(dir.value)) & notAFile;
			break;
		case S:
			shifted = board >>> Math.abs(dir.value);
			break;
		case SW:
			shifted = (board >>> Math.abs(dir.value)) & notHFile;
			break;
		case W:
			shifted = (board >>> Math.abs(dir.value)) & notHFile;
			break;
		case NW:
			shifted = (board << Math.abs(dir.value)) & notHFile;
			break;
		}
		return shifted;
	}

	/*
	 * Shift board by number of rows specified by shift.
	 * If shift < 0, shift board to down.
	 * If shift > 0, shift board to up.
	 */
	public static long shiftBoardByRow(long board, int shift) {
		long shiftedBoard;
		if (shift < 0) {
			shiftedBoard = board >>> -shift * 8;
		} else if (shift == 0) {
			shiftedBoard = board;
		} else {
			shiftedBoard = board << shift * 8;
		}
		return shiftedBoard;
	}

	/*
	 * Shift board by number of columns specified by colShift.
	 * If shift < 0, shift board to left.
	 * If shift > 0, shift board to right.
	 */
	public static long shiftBoardByColumn(long board, int shift) {
		long shiftedBoard;
		long shiftMask = 0;
		if (shift < 0) {
			// shifting left
			// have to mask out bits on right side that
			// were shifted into right side
			// If we are shifting two columns to left,
			// then right two columns will be zeros
			for (int i = 0; i < -shift; ++i) {
				shiftMask |= cols[7 - i];
			}
			shiftedBoard = (board >>> -shift) & ~shiftMask;
		} else if (shift == 0) {
			shiftedBoard = board;
		} else {
			for (int i = 0; i < shift; ++i) {
				shiftMask |= cols[i];
			}
			shiftedBoard = (board << shift) & ~shiftMask;
		}
		return shiftedBoard;
	}

	private static void generateSquares() {
		long bb = 1;
		for (int i = 0; i < squares.length; ++i) {
			squares[i] = bb;
			bb = bb << 1;
		}
	}

	public static long[] getKnightMovesMask() {
		return knightMovesMask;
	}

	public static long[] getKingMovesMask() {
		return kingMovesMask;
	}

	public static long[] getBishopmovesmask() {
		return bishopMovesMask;
	}

	public static long[] getRookmovesmask() {
		return rookMovesMask;
	}

	public static long[] getRookmagic() {
		return rookMagic;
	}

	public static long[] getBishopmagic() {
		return bishopMagic;
	}

	public static int[] getRookbitshift() {
		return rookBitShift;
	}

	public static int[] getBishopbitshift() {
		return bishopBitShift;
	}

	public static long[][] getBishopmoves() {
		return bishopMoves;
	}

	public static long[][] getRookmoves() {
		return rookMoves;
	}

	public static long[] getRows() {
		return rows;
	}

	public static long[] getCols() {
		return cols;
	}

	public static long[] getSquares() {
		return squares;
	}

	public static long[][] getPawnattacksmask() {
		return pawnAttacksMask;
	}

}
