package ca.tfuller.chessics.bitboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BoardOps extends StateOps {

	private static final Logger logger = LogManager.getLogger(BoardOps.class);

	//@formatter:off

	/*
	 * here are the longs stored in the board array
	 
	long white;
	long black;
	long occupied;
	long empty;
	long pawns;
	long knights;
	long bishops;
	long rooks;
	long queens;
	long kings;
	long state;
	long pieces
	*/

	static final int wPawnsIdx 	 = PieceEnum.WPAWN.getBbi();
	static final int wNightsIdx  = PieceEnum.WNIGHT.getBbi();
	static final int wBishopsIdx = PieceEnum.WBISHOP.getBbi();
	static final int wRooksIdx 	 = PieceEnum.WROOK.getBbi();
	static final int wQueensIdx  = PieceEnum.WQUEEN.getBbi();
	static final int wKingsIdx 	 = PieceEnum.WKING.getBbi();
	static final int bPawnsIdx 	 = PieceEnum.BPAWN.getBbi();
	static final int bNightsIdx  = PieceEnum.BNIGHT.getBbi();
	static final int bBishopsIdx = PieceEnum.BBISHOP.getBbi();
	static final int bRooksIdx 	 = PieceEnum.BROOK.getBbi();
	static final int bQueensIdx  = PieceEnum.BQUEEN.getBbi();
	static final int bKingsIdx 	 = PieceEnum.BKING.getBbi();
	static final int stateIdx 	 = bKingsIdx+1;
	static final int bitboardsArraySize = stateIdx+1;
	static final long[] zeroBoards = new long[stateIdx+1];
	
	// private constructor
	// no one needs to build this class
	// all methods are private
	private BoardOps() {
		
	}
	
	//@formatter:on

	public static void main(String[] args) {
		BoardOps bo = new BoardOps();

		/*
		logger.info("white");
		Utils.printBoard(bo.bitboards[whiteIdx]);
		logger.info("black");
		Utils.printBoard(bo.bitboards[blackIdx]);
		logger.info("occupied");
		Utils.printBoard(bo.bitboards[occupiedIdx]);
		logger.info("empty");
		Utils.printBoard(bo.bitboards[emptyIdx]);
		logger.info("pawns");
		Utils.printBoard(bo.bitboards[pawnsIdx]);
		logger.info("knights");
		Utils.printBoard(bo.bitboards[knightsIdx]);
		logger.info("bhshops");
		Utils.printBoard(bo.bitboards[bishopsIdx]);
		logger.info("rooks");
		Utils.printBoard(bo.bitboards[rooksIdx]);
		logger.info("queens");
		Utils.printBoard(bo.bitboards[queensIdx]);
		logger.info("kings");
		Utils.printBoard(bo.bitboards[kingsIdx]);
		*/

		logger.info(bo.toString());
	}

	public static long[] allocate() {
		long[] board = new long[bitboardsArraySize];
		return board;
	}

	public static long[] copy(long[] bitboards) {
		long[] board = new long[bitboardsArraySize];
		System.arraycopy(bitboards, 0, board, 0, bitboardsArraySize);
		return board;
	}

	public static void initialPosition(long[] bitboards) {
		//@formatter:off
		bitboards[wPawnsIdx]   = 0x000000000000ff00l;
		bitboards[wRooksIdx]   = 0b0000000000000000000000000000000000000000000000000000000010000001l;
		bitboards[wNightsIdx]  = 0b0000000000000000000000000000000000000000000000000000000001000010l;
		bitboards[wBishopsIdx] = 0b0000000000000000000000000000000000000000000000000000000000100100l;
		bitboards[wQueensIdx]  = 0b0000000000000000000000000000000000000000000000000000000000001000l;
		bitboards[wKingsIdx]   = 0b0000000000000000000000000000000000000000000000000000000000010000l;
		bitboards[bPawnsIdx]   = 0x00ff000000000000l;
		bitboards[bRooksIdx]   = 0b1000000100000000000000000000000000000000000000000000000000000000l;
		bitboards[bNightsIdx]  = 0b0100001000000000000000000000000000000000000000000000000000000000l;
		bitboards[bBishopsIdx] = 0b0010010000000000000000000000000000000000000000000000000000000000l;
		bitboards[bQueensIdx]  = 0b0000100000000000000000000000000000000000000000000000000000000000l;
		bitboards[bKingsIdx]   = 0b0001000000000000000000000000000000000000000000000000000000000000l;
		//@formatter:on

		initializeState(bitboards);
	}

	public static long getWhite(long[] bitboards) {
		long bb = bitboards[wPawnsIdx] | bitboards[wNightsIdx] | bitboards[wBishopsIdx] | bitboards[wRooksIdx]
				| bitboards[wQueensIdx] | bitboards[wKingsIdx];
		return bb;
	}

	public static long getBlack(long[] bitboards) {
		long bb = bitboards[bPawnsIdx] | bitboards[bNightsIdx] | bitboards[bBishopsIdx] | bitboards[bRooksIdx]
				| bitboards[bQueensIdx] | bitboards[bKingsIdx];
		return bb;
	}

	public static long getOccupied(long[] bitboards) {
		return getWhite(bitboards) | getBlack(bitboards);
	}

	/*
	 * Return an array of 1D indexes for given piece type and color.
	 */
	public static int[] getPieceIndexes(long[] bitboards, PieceEnum piece) {
		int[] locations;
		long pieceboard = 0;

		pieceboard = bitboards[piece.getBbi()];

		int pieces = Long.bitCount(pieceboard);
		locations = new int[pieces];

		int locationIndex = 0;
		for (int bitindex = 0; bitindex < 64; ++bitindex) {
			long lsbMask = 1l;
			long lsb = pieceboard & lsbMask;
			if (lsb == 1) {
				locations[locationIndex] = bitindex;
				++locationIndex;
			}
		}

		return locations;

	}

	/*
	public long getWhitePieces() {
		return bitboards[whiteIdx];
	}
	
	public long getBlackPieces() {
		return bitboards[blackIdx];
	}
	
	public long getOccupied() {
		return bitboards[occupiedIdx];
	}
	
	public long getEmpty() {
		return bitboards[emptyIdx];
	}
	*/

	/*
	 * Return an int 1D array of length 64 with values set to the piece enum values.
	 */
	public static byte[] getSquares(long[] bitboards) {
		byte[] squares = new byte[64];

		for (PieceEnum piece : PieceEnum.values()) {
			long board;
			board = bitboards[piece.getBbi()];
			assignPiecesToSquares(bitboards, board, squares, piece);
		}
		return squares;
	}

	private static void assignPiecesToSquares(long[] bitboards, long board, byte[] squares, PieceEnum piece) {
		for (int i = 0; i < 64; ++i) {
			int bit = (int) (board & 1l);
			if (bit == 1) {
				squares[i] |= piece.getEncoding();
			}
			board >>>= 1l;
		}
	}

	public static void printBoard(long[] bitboards) {
		System.out.println(System.lineSeparator() + toString(bitboards));
	}

	/*
	 * Return a string representation of the board.
	 * Pieces are indicated with a char.
	 */
	public static String toString(long[] bitboards) {
		String s = "";
		byte[] squares = getSquares(bitboards);
		for (int row = 7; row >= 0; --row) {
			for (int col = 0; col <= 7; ++col) {
				int idx = Utils.getIndex(row, col);
				PieceEnum piece = PieceEnum.fromEncoding(squares[idx]);
				char c = piece.toChar();
				s += c;
			}
			s += System.lineSeparator();
		}
		return s;
	}

	public static void clear(long[] bitboards) {
		System.arraycopy(zeroBoards, 0, bitboards, 0, bitboards.length);
	}

	public static void setBitboardsBySquares(long[] bitboards, byte[] squares) {
		// save state
		// TODO is state correct here?
		long state = bitboards[stateIdx];

		System.arraycopy(zeroBoards, 0, bitboards, 0, bitboardsArraySize);
		bitboards[stateIdx] = state;
		for (int idx = 0; idx < 64; ++idx) {
			PieceEnum piece = PieceEnum.fromEncoding(squares[idx]);
			int pieceboardIdx = piece.getBbi();
			bitboards[pieceboardIdx] |= BoardGenerator.squares[idx];
		}
	}

	public static String getMove(long[] bitboards1, long[] bitboards2) {
		long position1 = getOccupied(bitboards1);
		long position2 = getOccupied(bitboards2);

		long diff = position1 ^ position2;
		long from = diff & position1;
		long to = diff & position2;

		int fromIdx = Long.numberOfTrailingZeros(from);
		int toIdx = Long.numberOfTrailingZeros(to);

		String move = Utils.getCoordsString(fromIdx) + Utils.getCoordsString(toIdx);

		return move;
	}
}
