package ca.tfuller.chessics.bitboard;

public enum ColorEnum {
	WHITE(0, 'w'), BLACK(1, 'b'), NONE(-1, '.');

	private int value;
	private char letter;

	private ColorEnum(int value, char letter) {
		this.value = value;
		this.letter = letter;
	}

	public int getValue() {
		return value;
	}

	public char getLetter() {
		return letter;
	}

	public static ColorEnum getColorEnum(String color) {
		for (ColorEnum colorEnum : ColorEnum.values()) {
			if (colorEnum.toString().substring(0, 1).equalsIgnoreCase(color.substring(0, 1))) {
				return colorEnum;
			}
		}
		return null;
	}

	public ColorEnum getOppositeColor() {
		if (this.equals(BLACK)) {
			return WHITE;
		}
		return BLACK;
	}

	public static ColorEnum getOppositeColor(ColorEnum color) {
		ColorEnum oppositeColor = BLACK;
		if (color.equals(oppositeColor)) {
			oppositeColor = WHITE;
		}
		return oppositeColor;
	}
}
