package ca.tfuller.chessics.bitboard;

public enum DirectionEnum {
	N(8), NE(9), E(1), SE(-7), S(-8), SW(-9), W(-1), NW(7);

	int value;

	DirectionEnum(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}
