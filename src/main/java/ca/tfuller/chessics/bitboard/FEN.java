package ca.tfuller.chessics.bitboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FEN {

	private static final Logger logger = LogManager.getLogger(FEN.class);

	// set the pieces given board position in fen
	public static void setPosition(long[] bitboards, String fen) {
		BoardOps.clear(bitboards);
		byte[] squares = new byte[64];

		String[] lineStrings = fen.split(" ");
		String[] rowStrings = lineStrings[0].split("/");
		int row = 7;

		for (String rowString : rowStrings) {
			int col = 0;
			for (int i = 0; i < rowString.length(); ++i) {
				char c = rowString.charAt(i);
				if ('1' <= c && c <= '8') {
					col += c - '1' + 1;
				} else {
					PieceEnum piece = PieceEnum.fromChar(c);
					int idx = Utils.getIndex(row, col);
					squares[idx] = piece.getEncoding();
					++col;
				}
			}
			--row;
		}

		String castlingString = lineStrings[2];
		StateOps.setCastle(bitboards, castlingString);

		if (lineStrings[3].equals("-")) {
			StateOps.clearEnpassant(bitboards);
		} else {
			StateOps.setEnpassantSquare(bitboards, Utils.getIndex(lineStrings[3]));
		}

		StateOps.setHalfMoveClock(bitboards, Integer.parseInt(lineStrings[4]));

		// get the full move number
		StateOps.setFullMoveNumber(bitboards, Integer.parseInt(lineStrings[5]));

		// who is to move?
		StateOps.setColor(bitboards, lineStrings[1].charAt(0));
		BoardOps.setBitboardsBySquares(bitboards, squares);

	}

	public static String toString(long[] bitboards) {
		String s = "";
		byte[] squares = BoardOps.getSquares(bitboards);

		for (int row = 7; row >= 0; --row) {
			int col = 0;
			while (col <= 7) {
				int index = Utils.getIndex(row, col);

				byte pieceEncoding = squares[index];
				PieceEnum piece = PieceEnum.fromEncoding(pieceEncoding);
				if (piece != PieceEnum.EMPTY) {
					char c = piece.toChar();
					s += c;
					++col;
				} else {
					int emptySquares = 0;
					while (col <= 7 && squares[index] == 0) {
						++emptySquares;
						++col;
						index = Utils.getIndex(row, col);
					}
					s += Integer.toString(emptySquares);
				}
			}
			if (row != 0) {
				s += "/";
			}
		}
		s += " " + BoardOps.getColor(bitboards).getLetter();

		// check castling ability
		String castle = "";
		if (StateOps.getKingsideWhite(bitboards)) {
			castle += "K";
		}
		if (StateOps.getQueensideWhite(bitboards)) {
			castle += "Q";
		}
		if (StateOps.getKingsideBlack(bitboards)) {
			castle += "k";
		}
		if (StateOps.getQueensideBlack(bitboards)) {
			castle += "q";
		}

		if (castle.isEmpty()) {
			castle = "-";
		}
		s += " " + castle;

		if (StateOps.getEnpassantPossible(bitboards)) {
			s += " " + Utils.getCoordsString(StateOps.getEnpassantSquare(bitboards));
		} else {
			s += " -";
		}

		s += " " + Integer.toString(StateOps.getHalfMoveClock(bitboards));
		s += " " + Integer.toString(StateOps.getFullMoveNumber(bitboards));
		return s;
	}
}
