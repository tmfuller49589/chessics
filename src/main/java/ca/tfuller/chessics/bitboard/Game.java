package ca.tfuller.chessics.bitboard;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Game {

	private List<String> fenList = new ArrayList<String>();
	private static final Logger logger = LogManager.getLogger(Game.class);

	private List<long[]> gameBitboards = new ArrayList<long[]>();

	public List<String> getFenList() {
		return fenList;
	}

	public void newGame() {
		fenList.clear();
		gameBitboards.clear();
		long[] bitboards = BoardOps.allocate();
		gameBitboards.add(bitboards);
		BoardOps.initialPosition(getLastBitboards());
		fenList.add(FEN.toString(getLastBitboards()));
	}

	public void loadGame(File file) throws IOException {
		logger.info("loading fen game file " + file.getAbsolutePath());
		List<String> lines;
		lines = Files.readAllLines(file.toPath());
		fenList.clear();
		for (String line : lines) {
			logger.info("line is ==>" + line + "<==");
			fenList.add(line);
			long[] bitboards = BoardOps.allocate();

			FEN.setPosition(bitboards, line);
			gameBitboards.add(bitboards);
		}
	}

	/*
	 * Does a move from source to destination specified by 1D array index.
	 * Also does some book keeping: update the move and half move clocks, update the FEN list
	 *  
	 */
	public void movePiece(int source, int destination, PieceEnum pawnPromotionXXX) {
		// TODO handle pawn promo!

		long[] newBitboards = BoardOps.copy(getLastBitboards());
		gameBitboards.add(newBitboards);

		logger.info("newBitboards: " + newBitboards.hashCode());
		logger.info("getLastBitboards(): " + getLastBitboards().hashCode());

		byte squares[] = BoardOps.getSquares(getLastBitboards());

		int capturedSquare = -1;
		PieceEnum piece = PieceEnum.fromEncoding(squares[source]);

		if (squares[destination] != 0) {
			capturedSquare = destination;
		}
		// clear the source and destination board squares
		PieceEnum sourcePiece = PieceEnum.fromEncoding(squares[source]);
		squares[source] = PieceEnum.EMPTY.getEncoding();
		squares[destination] = PieceEnum.EMPTY.getEncoding();
		logger.info("after clearing squares");
		Utils.printBoard(squares);

		if (piece.equals(PieceEnum.WKING) || piece.equals(PieceEnum.BKING)) {
			PieceEnum rook;

			if (piece.equals(PieceEnum.WKING)) {
				BoardOps.clearCastleWhite(newBitboards);
				rook = PieceEnum.WROOK;
			} else {
				BoardOps.clearCastleBlack(newBitboards);
				rook = PieceEnum.BROOK;
			}

			// check for castle
			if (Math.abs(Utils.getColIdx(source) - Utils.getColIdx(destination)) == 2) {
				// move the rook
				int rookRow = 0;
				int rookSourceCol = 0;
				int rookDestCol = 0;

				if (BoardOps.getColor(newBitboards).equals(ColorEnum.WHITE)) {
					rookRow = 0;
				} else {
					rookRow = 7;
				}
				if (Utils.getColIdx(destination) > Utils.getColIdx(source)) {
					// king side castle
					rookSourceCol = 7;
					rookDestCol = 5;
				} else {
					// queen side castle
					rookSourceCol = 0;
					rookDestCol = 3;
				}
				int rookSource = Utils.getIndex(rookRow, rookSourceCol);
				squares[rookSource] = 0;
				int rookDestination = Utils.getIndex(rookRow, rookDestCol);
				squares[rookDestination] = rook.getEncoding();
			}
			BoardOps.clearEnpassant(newBitboards);
		} else if (piece.equals(PieceEnum.WROOK) || piece.equals(PieceEnum.BROOK)) {
			if (Utils.getColIdx(source) == 0) {
				if (BoardOps.getColor(newBitboards).equals(ColorEnum.BLACK)) {
					BoardOps.clearQueensideBlack(newBitboards);
				} else {
					BoardOps.clearQueensideWhite(newBitboards);
				}
			} else if (Utils.getColIdx(source) == 7) {
				if (BoardOps.getColor(newBitboards).equals(ColorEnum.BLACK)) {
					BoardOps.clearKingsideBlack(newBitboards);
				} else {
					BoardOps.clearKingsideWhite(newBitboards);
				}
			}
			BoardOps.clearEnpassant(newBitboards);
		} else if ((piece.equals(PieceEnum.WPAWN) || piece.equals(PieceEnum.BPAWN))
				&& destination == BoardOps.getEnpassantSquare(newBitboards)) {
			// enPassant capture
			int col = Utils.getColIdx(BoardOps.getEnpassantSquare(newBitboards));
			int row;
			if (BoardOps.getColor(newBitboards).equals(ColorEnum.WHITE)) {
				row = 4;
			} else {
				row = 3;
			}

			logger.info("en passant square is " + Utils.getRowIdx(BoardOps.getEnpassantSquare(newBitboards)) + " "
					+ Utils.getColIdx(BoardOps.getEnpassantSquare(newBitboards)));
			logger.info("en passant capture row = " + row + " col = " + col);
			capturedSquare = Utils.getIndex(row, col);

			squares[capturedSquare] = 0;
			BoardOps.clearEnpassant(newBitboards);
		} else if ((piece.equals(PieceEnum.WPAWN) || piece.equals(PieceEnum.BPAWN))
				&& Math.abs(Utils.getRowIdx(source) - Utils.getRowIdx(destination)) == 2) {
			// pawn advanced two squares, set en passant capture square
			int enPassantCol = Utils.getColIdx(source);
			int enPassantRow;
			if (BoardOps.getColor(newBitboards).equals(ColorEnum.WHITE)) {
				enPassantRow = 2;
			} else {
				enPassantRow = 5;
			}
			BoardOps.setEnpassantSquare(newBitboards, Utils.getIndex(enPassantRow, enPassantCol));
		}

		squares[destination] = sourcePiece.getEncoding();
		Utils.printBoard(squares);

		if (piece.equals(PieceEnum.WPAWN) || piece.equals(PieceEnum.BPAWN) || capturedSquare != -1) {
			BoardOps.setHalfMoveClock(newBitboards, 0);
		} else {
			BoardOps.incHalfMoveClock(newBitboards);
		}

		if (BoardOps.getColor(newBitboards).equals(ColorEnum.BLACK)) {
			BoardOps.incFullMoveNumber(newBitboards);
		}

		BoardOps.switchColor(getLastBitboards());
		logger.info("current color is " + BoardOps.getColor(getLastBitboards()));
		BoardOps.setBitboardsBySquares(getLastBitboards(), squares);
		String fen = FEN.toString(getLastBitboards());
		fenList.add(fen);
		logger.info("xxxfen: " + fen);
		Utils.printBoard(BoardOps.getSquares(newBitboards));
	}

	public String getMoveList() {
		String moves = "";
		for (int i = 0; i < getFenList().size() - 1; ++i) {

			if (i % 2 == 1) {
				moves += " ";
			} else {
				moves += (i / 2) + 1 + ". ";
			}
			moves += BoardOps.getMove(getGameBitboards().get(i), getGameBitboards().get(i + 1));

			if (i % 2 == 1) {
				moves += System.lineSeparator();
			}
		}
		return moves;
	}

	public void printFenList() {
		logger.info("printing FEN list");
		for (String s : fenList) {
			logger.info(s);
		}
	}

	public List<long[]> getGameBitboards() {
		return gameBitboards;
	}

	public long[] getLastBitboards() {
		return gameBitboards.get(gameBitboards.size() - 1);
	}

	/*
	 * A piece was moved from source to dest.
	 * Undo the move.
	 */
	public void takeBack() {
		fenList.remove(fenList.size() - 1);
		gameBitboards.remove(gameBitboards.size() - 1);
	}
}
