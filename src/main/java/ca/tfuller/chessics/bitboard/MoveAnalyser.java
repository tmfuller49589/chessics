package ca.tfuller.chessics.bitboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MoveAnalyser {
	private static final Logger logger = LogManager.getLogger(MoveAnalyser.class);

	long occupied;
	long empty;
	long white;
	long black;
	long opponentColorBoard;
	long currentPlayerColorBoard;
	long[] bitboards;
	byte[] squares;
	static long bPawnRank = 0x00FF000000000000l;
	static long wPawnRank = 0x000000000000FF00l;
	static long bPawnRank2 = 0x000000FF00000000l;
	static long wPawnRank2 = 0x00000000FF000000l;

	public void setBitboards(long[] bitboards, byte[] squares) {
		this.bitboards = bitboards;
		this.squares = squares;

		white = bitboards[BoardOps.wPawnsIdx] + bitboards[BoardOps.wNightsIdx] + bitboards[BoardOps.wBishopsIdx]
				+ bitboards[BoardOps.wRooksIdx] + bitboards[BoardOps.wQueensIdx] + bitboards[BoardOps.wKingsIdx];
		black = bitboards[BoardOps.bPawnsIdx] + bitboards[BoardOps.bNightsIdx] + bitboards[BoardOps.bBishopsIdx]
				+ bitboards[BoardOps.bRooksIdx] + bitboards[BoardOps.bQueensIdx] + bitboards[BoardOps.bKingsIdx];
		occupied = white | black;
		empty = ~occupied;
		if (BoardOps.getColor(bitboards).equals(ColorEnum.WHITE)) {
			opponentColorBoard = black;
			currentPlayerColorBoard = white;
		} else {
			opponentColorBoard = white;
			currentPlayerColorBoard = black;
		}

	}

	public boolean checkForMate() {
		return false;
	}

	/*
	 * Given an index into the 1D array of squares,
	 * get the piece that occupies that square.
	 * Find all the legal moves that the piece can occupy.
	 * Return an array of indices into the squares array.
	 */
	public int[] findLegalSquares(int squareIndex) {
		// TODO castling
		// TODO is king in check

		PieceEnum piece = PieceEnum.fromEncoding(squares[squareIndex]);

		logger.info("Piece is " + piece);
		logger.info("occupied");
		Utils.printBoard(occupied);
		logger.info("black");
		Utils.printBoard(black);
		logger.info("white");
		Utils.printBoard(white);
		logger.info("opponent");
		Utils.printBoard(opponentColorBoard);

		long legalBB = 0;
		switch (piece) {
		case BPAWN:
			long pawn = bitboards[BoardOps.bPawnsIdx] & BoardGenerator.squares[squareIndex];
			// pawn move forward one square
			long pawnAdvance = BoardGenerator.shiftBoardOne(pawn, DirectionEnum.S) & empty;
			long pawnAttack = BoardGenerator.shiftBoardOne(pawn, DirectionEnum.SE) & opponentColorBoard;
			pawnAttack |= BoardGenerator.shiftBoardOne(pawn, DirectionEnum.SW) & opponentColorBoard;
			legalBB = pawnAdvance | pawnAttack;

			// pawn move forward two squares
			if ((pawn & bPawnRank) != 0) {
				pawnAdvance = BoardGenerator.shiftBoardOne(pawnAdvance, DirectionEnum.S) & empty;
				legalBB |= pawnAdvance;
			}

			// check for possible enpassant capture
			if (BoardOps.getEnpassantPossible(bitboards)) {
				long epBoard = BoardGenerator.getSquares()[BoardOps.getEnpassantSquare(bitboards)];
				pawnAttack = BoardGenerator.shiftBoardOne(pawn, DirectionEnum.SE) & epBoard;
				pawnAttack |= BoardGenerator.shiftBoardOne(pawn, DirectionEnum.SW) & epBoard;
				legalBB |= pawnAttack;
			}
			break;
		case WPAWN:
			pawn = bitboards[BoardOps.wPawnsIdx] & BoardGenerator.squares[squareIndex];
			// pawn move forward one square
			pawnAdvance = BoardGenerator.shiftBoardOne(pawn, DirectionEnum.N) & empty;
			pawnAttack = BoardGenerator.shiftBoardOne(pawn, DirectionEnum.NE) & opponentColorBoard;
			pawnAttack |= BoardGenerator.shiftBoardOne(pawn, DirectionEnum.NW) & opponentColorBoard;
			legalBB = pawnAdvance | pawnAttack;

			// pawn move forward two squares
			if ((pawn & wPawnRank) != 0) {
				pawnAdvance = BoardGenerator.shiftBoardOne(pawnAdvance, DirectionEnum.N) & empty;
				legalBB |= pawnAdvance;
			}

			// check for possible enpassant capture
			if (BoardOps.getEnpassantPossible(bitboards)) {
				long epBoard = BoardGenerator.getSquares()[BoardOps.getEnpassantSquare(bitboards)];
				pawnAttack = BoardGenerator.shiftBoardOne(pawn, DirectionEnum.NE) & epBoard;
				pawnAttack |= BoardGenerator.shiftBoardOne(pawn, DirectionEnum.NW) & epBoard;
				legalBB |= pawnAttack;
			}
			break;
		case BNIGHT:
		case WNIGHT:
			legalBB = BoardGenerator.getKnightMovesMask()[squareIndex] & (empty | opponentColorBoard);
			break;
		case BBISHOP:
		case WBISHOP:
			legalBB = BoardGenerator.getBishopAttacks(occupied, squareIndex);
			break;
		case BROOK:
		case WROOK:
			legalBB = BoardGenerator.getRookAttacks(occupied, squareIndex);
			break;
		case BQUEEN:
		case WQUEEN:
			legalBB = BoardGenerator.getRookAttacks(occupied, squareIndex);
			legalBB |= BoardGenerator.getBishopAttacks(occupied, squareIndex);
			break;
		case BKING:
			legalBB = BoardGenerator.getKingMovesMask()[squareIndex] & (empty | opponentColorBoard);
			long king = bitboards[BoardOps.bPawnsIdx] & BoardGenerator.squares[squareIndex];
			legalBB |= getCastlingMoves(squareIndex, king, ColorEnum.BLACK);
			break;
		case WKING:
			legalBB = BoardGenerator.getKingMovesMask()[squareIndex] & (empty | opponentColorBoard);
			king = bitboards[BoardOps.bPawnsIdx] & BoardGenerator.squares[squareIndex];
			legalBB |= getCastlingMoves(squareIndex, king, ColorEnum.WHITE);
			break;
		case EMPTY:
			break;

		}

		// remove current player from capturing own pieces
		legalBB &= ~currentPlayerColorBoard;

		logger.info("legal moves");
		Utils.printBoard(legalBB);

		int[] indexes = new int[Long.bitCount(legalBB)];
		int iIndex = 0;
		for (int i = 0; i < 64 & iIndex < indexes.length; ++i) {
			if ((legalBB & 1l) == 1) {
				indexes[iIndex] = i;
				++iIndex;
			}
			legalBB >>>= 1;
		}
		return indexes;
	}

	public long getKingAttackedBy(int kingIdx, ColorEnum color) {
		long pawns, knights, rookQueen, bishopQueen;
		pawns = bitboards[PieceEnum.WPAWN.getBbi() + color.getValue()];
		knights = bitboards[PieceEnum.WNIGHT.getBbi() + color.getValue()];
		bishopQueen = bitboards[PieceEnum.WQUEEN.getBbi() + color.getValue()];
		rookQueen = bishopQueen;

		bishopQueen |= bitboards[PieceEnum.WBISHOP.getBbi() + color.getValue()];
		rookQueen |= bitboards[PieceEnum.WROOK.getBbi() + color.getValue()];

		long attacks = (BoardGenerator.getPawnattacksmask()[color.getValue()][kingIdx] & pawns)
				| (BoardGenerator.getKnightMovesMask()[kingIdx] & knights)
				| (BoardGenerator.getBishopAttacks(occupied, kingIdx) & bishopQueen)
				| (BoardGenerator.getRookAttacks(occupied, kingIdx) & rookQueen);
		return attacks;

	}

	public int getKingIndex(ColorEnum color) {
		long kingBB = bitboards[PieceEnum.WKING.getBbi() + color.getValue()];
		int kingIdx = Long.numberOfTrailingZeros(kingBB);
		logger.info("king index is " + kingIdx);
		return kingIdx;
	}

	public boolean isKingInCheck() {
		logger.info("current color is " + BoardOps.getColor(bitboards));
		int kingIdx = getKingIndex(BoardOps.getColor(bitboards));
		long kingAttackers = findAttackers(kingIdx) & opponentColorBoard;
		logger.info("king index = " + kingIdx);
		logger.info("opponents:");
		Utils.printBoard(opponentColorBoard);
		logger.info("king attackers " + kingAttackers);
		Utils.printBoard(kingAttackers);
		if (kingAttackers == 0) {
			return false;
		}
		return true;
	}

	private long getCastlingMoves(int squareIndex, long king, ColorEnum color) {
		long castleBitboard = 0;
		logger.info(FEN.toString(bitboards));

		switch (color) {
		case WHITE:
			if (BoardOps.getKingsideWhite(bitboards)) {
				logger.info("Checking kingside white castling squares");
				Utils.printBoard(findAttackers(2) & opponentColorBoard);
				Utils.printBoard(findAttackers(3) & opponentColorBoard);
				long castleThroughSquaresOccupied = (BoardGenerator.squares[5] | BoardGenerator.squares[6]) & occupied;

				logger.info("castle through squares = ");
				Utils.printBoard(castleThroughSquaresOccupied);
				if ((((findAttackers(5) | findAttackers(6)) & opponentColorBoard)
						| castleThroughSquaresOccupied) == 0) {
					castleBitboard = BoardGenerator.getSquares()[6];
				}
			}

			if (BoardOps.getQueensideWhite(bitboards)) {
				logger.info("Checking queenside white castling squares");
				Utils.printBoard(findAttackers(2) & opponentColorBoard);
				Utils.printBoard(findAttackers(3) & opponentColorBoard);
				long castleThroughSquaresOccupied = (BoardGenerator.squares[2] | BoardGenerator.squares[3]) & occupied;
				logger.info("castle through squares = ");
				Utils.printBoard(castleThroughSquaresOccupied);

				if ((((findAttackers(2) | findAttackers(3)) & opponentColorBoard)
						| castleThroughSquaresOccupied) == 0) {
					castleBitboard |= BoardGenerator.getSquares()[2];
				}
			}
			break;
		case BLACK:
			if (BoardOps.getKingsideBlack(bitboards)) {
				logger.info("Checking kingside black castling squares");
				Utils.printBoard(findAttackers(61) & opponentColorBoard);
				Utils.printBoard(findAttackers(62) & opponentColorBoard);
				long castleThroughSquaresOccupied = (BoardGenerator.squares[61] | BoardGenerator.squares[62])
						& occupied;
				logger.info("castle through squares = ");
				Utils.printBoard(castleThroughSquaresOccupied);
				if ((((findAttackers(61) | findAttackers(62)) & opponentColorBoard)
						| castleThroughSquaresOccupied) == 0) {
					castleBitboard = BoardGenerator.getSquares()[62];
				}
			}
			if (BoardOps.getQueensideBlack(bitboards)) {
				logger.info("Checking kingside black castling squares");
				Utils.printBoard(findAttackers(58) & opponentColorBoard);
				Utils.printBoard(findAttackers(59) & opponentColorBoard);
				long castleThroughSquaresOccupied = (BoardGenerator.squares[59] | BoardGenerator.squares[58])
						& occupied;
				logger.info("castle through squares = ");
				Utils.printBoard(castleThroughSquaresOccupied);
				if ((((findAttackers(59) | findAttackers(58)) & opponentColorBoard)
						| castleThroughSquaresOccupied) == 0) {
					castleBitboard |= BoardGenerator.getSquares()[58];
				}
			}
			break;
		case NONE:
			break;
		}

		return castleBitboard;
	}

	/*
	 * Find all pieces that attack a square.
	 * Create a super piece that has knight, bishop, rook, queen, kings
	 * Following the gist of https://www.chessprogramming.org/Square_Attacked_By
	 */
	public long findAttackers(int squareIndex) {
		long knights = bitboards[PieceEnum.BNIGHT.getBbi()] | bitboards[PieceEnum.WNIGHT.getBbi()];
		long kings = bitboards[PieceEnum.BKING.getBbi()] | bitboards[PieceEnum.WKING.getBbi()];
		long rooksQueens = bitboards[PieceEnum.BQUEEN.getBbi()] | bitboards[PieceEnum.WQUEEN.getBbi()];
		long bishopsQueens = rooksQueens;
		rooksQueens |= bitboards[PieceEnum.BROOK.getBbi()] | bitboards[PieceEnum.WROOK.getBbi()];
		bishopsQueens |= bitboards[PieceEnum.BBISHOP.getBbi()] | bitboards[PieceEnum.WBISHOP.getBbi()];

		long attacks = BoardGenerator.getKnightMovesMask()[squareIndex] & knights;
		attacks |= BoardGenerator.getKingMovesMask()[squareIndex] & kings;
		attacks |= BoardGenerator.getPawnattacksmask()[ColorEnum.WHITE.getValue()][squareIndex]
				& bitboards[PieceEnum.BPAWN.getBbi()];
		attacks |= BoardGenerator.getPawnattacksmask()[ColorEnum.BLACK.getValue()][squareIndex]
				& bitboards[PieceEnum.WPAWN.getBbi()];
		attacks |= BoardGenerator.getBishopAttacks(occupied, squareIndex) & bishopsQueens;
		attacks |= BoardGenerator.getRookAttacks(occupied, squareIndex) & rooksQueens;
		return attacks;
	}

}
