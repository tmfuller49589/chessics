package ca.tfuller.chessics.bitboard;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum PieceEnum {

	//@formatter:off
	EMPTY  (ColorEnum.NONE,  (byte) 0b00000000, '.', 0,  0),
	WPAWN  (ColorEnum.WHITE, (byte) 0b10000001, 'P', 1,  1),
	BPAWN  (ColorEnum.BLACK, (byte) 0b01000001, 'p', 2,  1),
	WNIGHT (ColorEnum.WHITE, (byte) 0b10000010, 'N', 3,  3),
	BNIGHT (ColorEnum.BLACK, (byte) 0b01000010, 'n', 4,  3),
	WBISHOP(ColorEnum.WHITE, (byte) 0b10000100, 'B', 5,  3),
	BBISHOP(ColorEnum.BLACK, (byte) 0b01000100, 'b', 6,  3),
	WROOK  (ColorEnum.WHITE, (byte) 0b10001000, 'R', 7,  5),
	BROOK  (ColorEnum.BLACK, (byte) 0b01001000, 'r', 8,  5),
	WQUEEN (ColorEnum.WHITE, (byte) 0b10010000, 'Q', 9,  9),
	BQUEEN (ColorEnum.BLACK, (byte) 0b01010000, 'q', 10, 9),
	WKING  (ColorEnum.WHITE, (byte) 0b10100000, 'K', 11,  100),
	BKING  (ColorEnum.BLACK, (byte) 0b01100000, 'k', 12, 100);

	private static final Logger logger = LogManager.getLogger(PieceEnum.class);
	private static final Map<Character, PieceEnum> fromCharMap = new HashMap<Character, PieceEnum>();
	private static final Map<Byte, PieceEnum> fromEncodingMap = new HashMap<Byte, PieceEnum>();
	static {
		for (PieceEnum piece : PieceEnum.values()) {
			fromCharMap.put(piece.toChar(), piece);
			fromEncodingMap.put(piece.getEncoding(), piece);
		}
	}
	

	//@formatter:on
	public static final byte colorMask = 0b1000;
	public static final byte pieceMask = 0b0111;

	private ColorEnum color;
	private byte encoding;
	private char letter;
	private int value;
	private int bbi;

	PieceEnum(ColorEnum color, byte encoding, char letter, int bbi, int value) {
		this.color = color;
		this.encoding = encoding;
		this.letter = letter;
		this.value = value;
		this.bbi = bbi;
	}

	public static void main(String[] args) {
		char c = 'P';
		PieceEnum p = fromChar(c);
		logger.info("char is " + c);
		logger.info("piece is " + p);
		logger.info("encoding is " + p.getEncoding());
		logger.info("encoding cast to int is " + (int) p.getEncoding());
		logger.info("piece is " + Utils.toBinaryString(p.getEncoding()));
		logger.info("piece is " + Utils.toBinaryString((int) p.getEncoding()));
	}

	public ColorEnum getColor() {
		return color;
	}

	public byte getEncoding() {
		return encoding;
	}

	public char toChar() {
		return letter;
	}

	public int getValue() {
		return value;
	}

	public int getBbi() {
		return bbi;
	}

	public static PieceEnum fromCharColor(char c, ColorEnum color) {
		if (color.equals(ColorEnum.WHITE)) {
			c = Character.toUpperCase(c);
		} else {
			c = Character.toLowerCase(c);
		}
		return fromCharMap.get(c);
	}

	public static PieceEnum fromChar(char c) {
		return fromCharMap.get(c);
	}

	public static PieceEnum fromEncoding(byte encoding) {
		return fromEncodingMap.get(encoding);
	}

	public static int getColor(Byte piece) {
		int color = piece & colorMask;
		return piece == 0 ? -1 : color >>> 3;
	}

	public static byte getPieceType(Byte piece) {
		return (byte) (piece & ~colorMask);
	}

	public static byte getPiece(ColorEnum color, int piece) {
		byte b = (byte) color.getValue();
		b = (byte) (b << 3);
		b |= piece;
		return b;
	}

}
