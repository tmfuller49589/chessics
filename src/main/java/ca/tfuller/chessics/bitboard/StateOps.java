package ca.tfuller.chessics.bitboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class StateOps {
	private static final Logger logger = LogManager.getLogger(StateOps.class);

	//@formatter:off
	//color bit: 0 = white, 1 = black
	static final long colorMask       = 0b10000000000000000000000000000000;
	static final long castlingMask    = 0b01111000000000000000000000000000;
	static final long queensideWhite  = 0b01000000000000000000000000000000;
	static final long kingsideWhite   = 0b00100000000000000000000000000000;
	static final long queensideBlack  = 0b00010000000000000000000000000000;
	static final long kingsideBlack   = 0b00001000000000000000000000000000;
	static final long enPassant       = 0b00000100000000000000000000000000;
	static final long enPassantSquare = 0b00000011111100000000000000000000;
	static final long halfMoveClock   = 0b00000000000011111111110000000000;
	static final long fullMoveNumber  = 0b00000000000000000000001111111111;
	static final long castleWhite = queensideWhite | kingsideWhite;
	static final long castleBlack = queensideBlack | kingsideBlack;

	static final int colorMaskShift       = Long.numberOfTrailingZeros(colorMask);
	static final int castlingMaskShift    = Long.numberOfTrailingZeros(castlingMask);
	static final int queensideWhiteShift  = Long.numberOfTrailingZeros(queensideWhite);
	static final int kingsideWhiteShift   = Long.numberOfTrailingZeros(kingsideWhite);
	static final int queensideBlackShift  = Long.numberOfTrailingZeros(queensideBlack);
	static final int kingsideBlackShift   = Long.numberOfTrailingZeros(kingsideBlack);
	static final int enPassantSquareShift = Long.numberOfTrailingZeros(enPassantSquare);
	static final int enPassantShift       = Long.numberOfTrailingZeros(enPassant);
	static final int halfMoveClockShift   = Long.numberOfTrailingZeros(halfMoveClock);
	static final int fullMoveNumberShift  = Long.numberOfTrailingZeros(fullMoveNumber);
	static final int castleWhiteShift     = Long.numberOfTrailingZeros(castleWhite);
	static final int castleBlackShift     = Long.numberOfTrailingZeros(castleBlack);
	
	// currently, long size is not needed, everything would fit inside int
	// but, I want to have one array for bitboards and state
	
	//@formatter:on

	public static void main(String[] args) {

		long[] bitboards = BoardOps.allocate();

		StateOps.setKingsideBlack(bitboards);
		StateOps.setQueensideBlack(bitboards);
		StateOps.setKingsideWhite(bitboards);
		StateOps.setQueensideWhite(bitboards);

		StateOps.setEnpassantSquare(bitboards, 24);
		StateOps.setColor(bitboards, ColorEnum.BLACK);
		StateOps.setHalfMoveClock(bitboards, 4);
		StateOps.setFullMoveNumber(bitboards, 18);

		StateOps.printState(bitboards);

		logger.info("clearing enpassant");
		StateOps.clearEnpassant(bitboards);
		StateOps.printState(bitboards);

		logger.info("incrementing half move clock");
		StateOps.incHalfMoveClock(bitboards);
		StateOps.printState(bitboards);

		logger.info("incrementing full move number");
		StateOps.incFullMoveNumber(bitboards);
		StateOps.printState(bitboards);

		logger.info("clearing black castle");
		StateOps.clearCastleBlack(bitboards);
		StateOps.printState(bitboards);

		logger.info("clearing white queenside");
		StateOps.clearQueensideWhite(bitboards);
		StateOps.printState(bitboards);

		logger.info("switching color");
		StateOps.switchColor(bitboards);
		StateOps.printState(bitboards);

		logger.info("setting enpassant");
		StateOps.setEnpassantSquare(bitboards, 52);
		StateOps.printState(bitboards);

	}

	public static void initializeState(long[] bitboards) {
		setKingsideBlack(bitboards);
		setQueensideBlack(bitboards);
		setKingsideWhite(bitboards);
		setQueensideWhite(bitboards);
	}

	public static void setColor(long[] bitboards, int value) {
		bitboards[BoardOps.stateIdx] &= ~colorMask;
		value <<= colorMaskShift;
		bitboards[BoardOps.stateIdx] |= value;
	}

	public static void setColor(long[] bitboards, char c) {
		if (c == 'w' || c == 'W') {
			setColor(bitboards, ColorEnum.WHITE.getValue());
		} else {
			logger.info("setting color to BLACK");
			setColor(bitboards, ColorEnum.BLACK.getValue());
		}
	}

	public static void setColor(long[] bitboards, ColorEnum color) {
		setColor(bitboards, color.getValue());
	}

	public static void switchColor(long[] bitboards) {
		logger.info("before shift, color is " + getColor(bitboards));
		bitboards[BoardOps.stateIdx] ^= 1 << colorMaskShift;
		logger.info("after shift, color is " + getColor(bitboards));
	}

	public static void setQueensideWhite(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~queensideWhite;
		bitboards[BoardOps.stateIdx] |= 1 << queensideWhiteShift;
	}

	public static void setKingsideWhite(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~kingsideWhite;
		bitboards[BoardOps.stateIdx] |= 1 << kingsideWhiteShift;
	}

	public static void setQueensideBlack(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~queensideBlack;
		bitboards[BoardOps.stateIdx] |= 1 << queensideBlackShift;
	}

	public static void setKingsideBlack(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~kingsideBlack;
		bitboards[BoardOps.stateIdx] |= 1 << kingsideBlackShift;
	}

	public static void clearKingsideBlack(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~kingsideBlack;
	}

	public static void clearQueensideBlack(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~queensideBlack;
	}

	public static void clearKingsideWhite(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~kingsideWhite;
	}

	public static void clearQueensideWhite(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~queensideWhite;
	}

	public static void clearCastleWhite(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~castleWhite;
	}

	public static void clearCastleBlack(long[] bitboards) {
		bitboards[BoardOps.stateIdx] &= ~castleBlack;
	}

	public static boolean getKingsideWhite(long[] bitboards) {
		int bit = (int) (bitboards[BoardOps.stateIdx] & kingsideWhite);
		return bit != 0;
	}

	public static boolean getQueensideWhite(long[] bitboards) {
		int bit = (int) (bitboards[BoardOps.stateIdx] & queensideWhite);
		return bit != 0;
	}

	public static boolean getKingsideBlack(long[] bitboards) {
		int bit = (int) (bitboards[BoardOps.stateIdx] & kingsideBlack);
		return bit != 0;
	}

	public static boolean getQueensideBlack(long[] bitboards) {
		int bit = (int) (bitboards[BoardOps.stateIdx] & queensideBlack);
		return bit != 0;
	}

	public static void clearEnpassant(long[] bitboards) {
		setEnpassant(bitboards, false);
	}

	public static void setEnpassantSquare(long[] bitboards, int index) {
		index <<= enPassantSquareShift;
		bitboards[BoardOps.stateIdx] &= ~enPassantSquare;
		bitboards[BoardOps.stateIdx] |= index;
		setEnpassant(bitboards, true);
	}

	public static void setEnpassant(long[] bitboards, boolean enabled) {
		int value = enabled ? 1 : 0;
		value <<= enPassantShift;
		bitboards[BoardOps.stateIdx] &= ~enPassant;
		bitboards[BoardOps.stateIdx] |= value;
	}

	public static boolean getEnpassantPossible(long[] bitboards) {
		int square = (int) (bitboards[BoardOps.stateIdx] & enPassant);
		square >>>= enPassantShift;
		return square != 0;
	}

	public static int getEnpassantSquare(long[] bitboards) {
		int square = (int) (bitboards[BoardOps.stateIdx] & enPassantSquare);
		square >>>= enPassantSquareShift;
		if (getEnpassantPossible(bitboards)) {
			return square;
		}
		return -1;
	}

	public static void setHalfMoveClock(long[] bitboards, int value) {
		value <<= halfMoveClockShift;
		bitboards[BoardOps.stateIdx] &= ~halfMoveClock;
		bitboards[BoardOps.stateIdx] |= value;

	}

	public static void incHalfMoveClock(long[] bitboards) {
		int hmc = getHalfMoveClock(bitboards);
		++hmc;
		setHalfMoveClock(bitboards, hmc);
	}

	public static int getHalfMoveClock(long[] bitboards) {
		int hmc = (int) (bitboards[BoardOps.stateIdx] & halfMoveClock);
		hmc >>>= halfMoveClockShift;
		return hmc;
	}

	public static void setFullMoveNumber(long[] bitboards, int value) {
		bitboards[BoardOps.stateIdx] &= ~fullMoveNumber;
		bitboards[BoardOps.stateIdx] |= value;
	}

	public static void incFullMoveNumber(long[] bitboards) {
		int fmn = getFullMoveNumber(bitboards);
		++fmn;
		setFullMoveNumber(bitboards, fmn);
	}

	public static int getFullMoveNumber(long[] bitboards) {
		int hmc = (int) (bitboards[BoardOps.stateIdx] & fullMoveNumber);
		return hmc;
	}

	public static ColorEnum getColor(long[] bitboards) {
		int color = (int) (bitboards[BoardOps.stateIdx] & colorMask);
		color >>>= colorMaskShift;
		if (color == ColorEnum.BLACK.getValue()) {
			return ColorEnum.BLACK;
		}
		return ColorEnum.WHITE;
	}

	public static void setCastle(long[] bitboards, String s) {
		for (int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);
			switch (c) {
			case 'Q':
				setQueensideWhite(bitboards);
				break;
			case 'K':
				setKingsideWhite(bitboards);
				break;
			case 'q':
				setQueensideBlack(bitboards);
				break;
			case 'k':
				setKingsideBlack(bitboards);
				break;
			}
		}
	}

	public static void printState(long[] bitboards) {
		logger.info("================================================================================");
		logger.info("FEN: " + FEN.toString(bitboards));
		logger.info("bitboards[BoardOps.stateIdx] string is " + Utils.toBinaryString(bitboards[BoardOps.stateIdx]));
		logger.info("active color = " + getColor(bitboards));
		logger.info("white kingside castle = " + getKingsideWhite(bitboards));
		logger.info("white queenside castle = " + getQueensideWhite(bitboards));
		logger.info("black kingside castle = " + getKingsideBlack(bitboards));
		logger.info("black queenside castle = " + getKingsideBlack(bitboards));
		logger.info("en passant square = " + getEnpassantSquare(bitboards));
		logger.info("half move clock = " + getHalfMoveClock(bitboards));
		logger.info("full move number = " + getFullMoveNumber(bitboards));
		logger.info("================================================================================");

	}

}
