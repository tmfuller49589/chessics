package ca.tfuller.chessics.bitboard;

public class ThirtyTwoBitMult {

	// comparing multiplies from https://www.chessprogramming.org/Looking_for_Magics
	public static int mult32(long b, long magic, int bits) {

		// (unsigned)((int)b*(int)magic ^ (int)(b>>32)*(int)(magic>>32)) >> (32-bits);

		int a = (int) b * (int) magic;
		int c = (int) (b >>> 32) * (int) (magic >>> 32);
		int d = a ^ c;
		int e = d >> 32;
		return e;

	}

	public static long mult64(long b, long magic, int bits) {
		long bm = b * magic;
		System.out.println(Utils.toBinaryString(bm));
		long bms = bm >>> (64 - bits);
		return bms;
	}

	public static void main(String[] args) {
		int b = 23414;
		int magic = 1242;
		int bits = 12;
		long m64 = mult64(b, magic, bits);
		int m32 = mult32(b, magic, bits);
		System.out.println("mult32: " + m32);
		System.out.println("mult64: " + m64);

		long ll = 0x1E0DF99F0AD9F149L;
		System.out.println(ll);
		System.out.println((int) ll);

		System.out.println(System.getProperty("java.version"));

	}
}
