package ca.tfuller.chessics.bitboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {

	private static final Logger logger = LogManager.getLogger(Utils.class);

	public static final int getRowIdx(int idx) {
		return idx / 8;
	}

	public static final int getColIdx(int idx) {
		return idx % 8;
	}

	public static final int getIndex(int row, int col) {
		return row * 8 + col;
	}

	public static String getCoordsString(int index) {
		int col = getColIdx(index);
		int row = getRowIdx(index);
		char colChar = (char) ('a' + col);
		return Character.toString(colChar) + (row + 1);
	}

	public static long clearBit(long b, int bit) {
		long setter = 1L << bit;
		setter = ~setter;
		return b & setter;
	}

	public static long clearBit(long b, int row, int col) {
		int bit = getIndex(row, col);
		long setter = 1L << bit;
		setter = ~setter;
		return b & setter;
	}

	public static int getBit(long b, int bit) {
		return (b & 1L << bit) == 0 ? 0 : 1;
	}

	public static long setBit(long b, int bit, int value) {

		if (value == 1) {
			return setBit(b, bit);

		}
		return clearBit(b, bit);
	}

	public static long setBit(long b, int bit) {
		b |= 1L << bit;
		return b;
	}

	public static int flipBits(int n, int k) {
		int mask = (1 << k) - 1;
		return ~n & mask;
	}

	public static boolean checkBounds(final int rowTmp, final int colTmp) {

		if (0 <= rowTmp && rowTmp <= 7 && 0 <= colTmp && colTmp <= 7) {
			return true;
		}
		return false;
	}

	public static String toBinaryString(byte b) {
		String s = Integer.toBinaryString(b);
		if (s.length() > 8) {
			s = s.substring(s.length() - 8, s.length());
		}
		String pad0 = "";
		for (int i = 0; i < 8 - s.length(); ++i) {
			pad0 += "0";
		}
		return pad0 + s;

	}

	public static String toBinaryString(int b) {
		String s = Integer.toBinaryString(b);
		String pad0 = "";
		for (int i = 0; i < 32 - s.length(); ++i) {
			pad0 += "0";
		}
		return pad0 + s;

	}

	public static String toBinaryString(long b) {
		String s = Long.toBinaryString(b);
		String pad0 = "";
		for (int i = 0; i < 64 - s.length(); ++i) {
			pad0 += "0";
		}
		return pad0 + s;
	}

	public static void printBoard(byte[] squares) {
		String s = toBoardString(squares);
		logger.info("printing board by squares" + System.lineSeparator() + s);
	}

	public static String toBoardString(byte[] squares) {
		String s = "";
		for (int row = 7; row >= 0; --row) {
			for (int col = 0; col <= 7; ++col) {
				int idx = getIndex(row, col);
				PieceEnum piece = PieceEnum.fromEncoding(squares[idx]);
				s += piece.toChar();
			}
			s += System.lineSeparator();
		}
		return s;
	}

	public static void printBoard(long bb) {
		System.out.println("-----------------------------------");
		String s = Utils.toBinaryString(bb);
		System.out.println(s + " " + bb);
		for (int row = 0; row < 8; ++row) {
			for (int col = 7; col >= 0; --col) {
				int idx = row * 8 + col;
				char c = s.charAt(idx);
				System.out.print(c);
			}
			System.out.println();
		}
		System.out.println("-----------------------------------");
	}

	public static int getIndex(String coordString) {
		int row = getRow(coordString);
		int col = getCol(coordString);
		int index = getIndex(row, col);
		return index;
	}

	public static int getRow(String coordString) {
		int row = coordString.charAt(1) - '1';
		return row;
	}

	public static int getCol(String coordString) {
		int col = coordString.charAt(0) - 'a';
		return col;
	}

	public static String getCoordsString(int row, int col) {
		char colChar = (char) ('a' + col);
		return Character.toString(colChar) + (row + 1);
	}

}
