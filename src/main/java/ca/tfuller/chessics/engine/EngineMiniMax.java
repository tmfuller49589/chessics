package ca.tfuller.chessics.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.chessics.application.Piece;
import ca.tfuller.chessics.application.Square;
import ca.tfuller.chessics.bitboard.ColorEnum;
import ca.tfuller.chessics.bitboard.PieceTypeEnum;
import ca.tfuller.chessics.chess.BoardOpsOld;
import ca.tfuller.chessics.chess.MoveAnalyser;

public class EngineMiniMax {
	protected static final Logger logger = LogManager.getLogger(EngineMiniMax.class);

	private int depthMax = 4; // number of plies to look ahead

	private final int maxScore = 1000;
	private final int minScore = -maxScore;
	private MoveAnalyser moveAnalyser;
	private static int positionsConsidered = 0;

	private ColorEnum computer;
	private BoardOpsOld originalBoard;
	private Piece computerQueen;
	private Piece playerQueen;
	private int maxThreads = 20;

	static Square bestMoveSource = null;
	static Square bestMoveDestination = null;
	static int bestMoveValue = -Integer.MIN_VALUE;

	public EngineMiniMax(ColorEnum computer, MoveAnalyser moveAnalyser, BoardOpsOld originalBoard) {
		super();
		this.moveAnalyser = moveAnalyser;
		this.computer = computer;
		this.originalBoard = originalBoard;
		computerQueen = new Piece(PieceTypeEnum.Queen, computer); // used for promotions
		playerQueen = new Piece(PieceTypeEnum.Queen, computer.getOppositeColor()); // used for promotions

	}

	public void bestMove(ColorEnum color) {
		EnumMap<ColorEnum, ArrayList<Square>> occupiedSquaresBothColors = moveAnalyser.findPieces(originalBoard);
		ArrayList<Square> squares = occupiedSquaresBothColors.get(color);
		logger.info("color is " + color + " occupied squares is " + squares);

		boolean threaded = false;
		Long startTime = System.currentTimeMillis();

		if (threaded == false) {
			for (Square square : squares) {
				BoardOpsOld boardCopy = originalBoard.copy();
				Square squareToMove = boardCopy.getSquares()[square.getRow()][square.getCol()];
				ThreadedMiniMax threadedMiniMax = new ThreadedMiniMax(boardCopy, moveAnalyser, squareToMove);
				logger.info("adding " + square + " to task list");
				threadedMiniMax.run();
			}
		} else {
			ThreadPoolExecutor tpe = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxThreads);
			Collection<Future<?>> tasks = new LinkedList<Future<?>>();

			for (Square square : squares) {
				BoardOpsOld boardCopy = originalBoard.copy();
				Square squareToMove = boardCopy.getSquares()[square.getRow()][square.getCol()];
				ThreadedMiniMax threadedMiniMax = new ThreadedMiniMax(boardCopy, moveAnalyser, squareToMove);
				logger.info("adding " + square + " to task list");
				Future future = tpe.submit(threadedMiniMax);
				tasks.add(future);

			}

			for (Future<?> currTask : tasks) {
				try {
					currTask.get();
					logger.info("i think a task completed");
				} catch (Throwable thrown) {
					thrown.printStackTrace();
					logger.error("Error while waiting for thread completion");
				}
			}
		}

		Long endTime = System.currentTimeMillis();
		double elapsedTime = (endTime - startTime) / 1000d;

		logger.info(
				"best move is from " + bestMoveSource + " to " + bestMoveDestination + " value is " + bestMoveValue);
		logger.info(positionsConsidered + " positions considered in " + elapsedTime + " seconds");
	}

	public int getDepthMax() {
		return depthMax;
	}

	public void setDepthMax(int depthMax) {
		this.depthMax = depthMax;
	}

	public static int getPositionsConsidered() {
		return positionsConsidered;
	}

	public static void setPositionsConsidered(int positionsConsidered) {
		EngineMiniMax.positionsConsidered = positionsConsidered;
	}

	class ThreadedMiniMax implements Runnable {

		private BoardOpsOld board;
		private Square squareToMove;

		public ThreadedMiniMax(BoardOpsOld board, MoveAnalyser moveAnalyser, Square square) {
			this.board = board;
			this.squareToMove = square;
		}

		@Override
		public void run() {
			Piece piece = squareToMove.getPiece();
			logger.info("piece is " + piece);
			logger.info("original square is " + squareToMove);

			Set<Square> legalSquares = moveAnalyser.findLegalSquares(board, squareToMove);
			for (Square s : legalSquares) {
				logger.info("trying legal square  " + s);

				// make the move
				board.movePiece(squareToMove, s, computerQueen);
				int moveVal = minimax(depthMax, false, Integer.MIN_VALUE, Integer.MAX_VALUE);
				logger.info("move val is " + moveVal);
				// undo the move
				logger.info("taking move back");

				board.takeBack();
				logger.info("after take back, original square is " + squareToMove);
				board.printBoard();

				synchronized (legalSquares) {
					if (moveVal > bestMoveValue) {
						bestMoveValue = moveVal;
						bestMoveSource = squareToMove;
						bestMoveDestination = s;
					}
				}
			}
		}

		int evaluate() {
			ColorEnum color = board.getCurrentPlayerColor();
			++positionsConsidered;
			if (moveAnalyser.checkForMate(board)) {
				if (color.equals(computer)) {
					return maxScore;
				}
				return minScore;
			}

			int whiteSum = 0;
			int blackSum = 0;

			for (Square[] row : board.getSquares()) {
				for (Square square : row) {
					Piece piece = square.getPiece();
					if (piece != null) {
						if (piece.getColor().equals(ColorEnum.Black)) {
							blackSum += piece.getType().getValue();
						} else {
							whiteSum += piece.getType().getValue();
						}
					}
				}
			}

			int value = 0;
			if (color.equals(computer)) {
				value = Math.abs(whiteSum - blackSum);
			} else {
				value = -Math.abs(whiteSum - blackSum);
			}

			return value;
		}

		int minimax(int depth, Boolean isMaximizingPlayer, int alpha, int beta) {

			if (depth == 0) {
				int score = evaluate();
				return score;
			}

			EnumMap<ColorEnum, ArrayList<Square>> occupiedSquares = moveAnalyser.findPieces(board);

			if (isMaximizingPlayer) {
				// maximizer's move
				int best = Integer.MIN_VALUE;
				for (Square source : occupiedSquares.get(computer)) {
					Set<Square> legitSquares = moveAnalyser.findLegalSquares(board, source);
					for (Square dest : legitSquares) {
						// make the move
						board.movePiece(source, dest, computerQueen);
						int mm = minimax(depth - 1, !isMaximizingPlayer, alpha, beta);
						board.takeBack();
						best = Math.max(best, mm);
						alpha = Math.max(alpha, best);
						if (alpha >= beta) {
							break;
						}
					}
				}

				return best;
			} else {
				// minimizer's move
				int best = Integer.MAX_VALUE;
				for (Square source : occupiedSquares.get(computer.getOppositeColor())) {
					Set<Square> legitSquares = moveAnalyser.findLegalSquares(board, source);
					for (Square dest : legitSquares) {
						// make the move
						board.movePiece(source, dest, playerQueen);
						int mm = minimax(depth - 1, !isMaximizingPlayer, alpha, beta);
						board.takeBack();
						best = Math.min(best, mm);
						beta = Math.min(beta, best);
						if (beta <= alpha) {
							break;
						}
					}
				}
				return best;
			}
		}
	}
}
