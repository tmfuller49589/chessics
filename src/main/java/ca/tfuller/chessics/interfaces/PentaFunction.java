package ca.tfuller.chessics.interfaces;

public interface PentaFunction<S, T, U, V, W, R> {
	R apply(S s, T t, U u, V v, W w);
}
