module chessics {
	opens ca.tfuller.chessics.application;

	exports ca.tfuller.chessics.chess;
	exports ca.tfuller.chessics.engine;
	exports ca.tfuller.chessics.interfaces;
	exports ca.tfuller.chessics.bitboard;
	exports animation;

	requires java.desktop;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	// requires org.junit.jupiter.api;
	requires java.base;
}