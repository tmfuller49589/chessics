package ca.tfuller.chessics.bitboard;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoardGeneratorTest {

	private static final Logger logger = LogManager.getLogger(BoardGeneratorTest.class);

	private static long blockerBoard;
	private static long pieceBoard;
	private static long firstBlockerBoard;
	private static long moveBoard;

	PieceEnum piece;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	// @Test
	void parseTest() {
		String s = "0000000000000000000000000000100000000000000000000000000000000000";
		long l = Long.parseLong(s, 2);
		logger.info("string is " + s);
		logger.info("long val is " + l);
	}

	// @Test
	void oneDirectionTest() {
		String filename = "bt4.txt";
		parseFile(filename);

		long computedMoveBoard = BoardGenerator.findFirstBlocker(blockerBoard, pieceBoard, DirectionEnum.E);
		logger.info("move board " + firstBlockerBoard);
		Utils.printBoard(firstBlockerBoard);
		logger.info("computed move board " + computedMoveBoard);
		Utils.printBoard(computedMoveBoard);

	}

	@Test
	void blockerTest() {

		URL pathUrl = BoardGeneratorTest.class.getClassLoader().getResource("blockertest/");
		// URL pathUrl = BoardGeneratorTest.class.getClassLoader().getResource("fen/");
		logger.info("path is " + pathUrl.getPath());
		if ((pathUrl != null) && pathUrl.getProtocol().equals("file")) {
			try {
				String[] pathnames = new File(pathUrl.toURI()).list();
				for (String s : pathnames) {
					testOneFile(s);
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	void testOneFile(String filename) {
		pieceBoard = 0;
		blockerBoard = 0;
		firstBlockerBoard = 0;

		logger.info("filename = " + filename);
		parseFile(filename);

		long computedFirstBlockerBoard = BoardGenerator.findFirstBlockers(blockerBoard, pieceBoard, piece);
		if (computedFirstBlockerBoard != firstBlockerBoard) {
			logger.info("error!! first blocker boards not matching " + filename);
			logger.info("first blocker board " + firstBlockerBoard);
			Utils.printBoard(firstBlockerBoard);
			logger.info("computed first blocker board " + computedFirstBlockerBoard);
			Utils.printBoard(computedFirstBlockerBoard);
		}
		assert (computedFirstBlockerBoard == firstBlockerBoard);

		long computedMoveBoard = BoardGenerator.fixEdges(computedFirstBlockerBoard, blockerBoard, pieceBoard, piece);
		if (computedMoveBoard != moveBoard) {
			logger.info("error!! move boards not matching " + filename);
			logger.info("move board " + moveBoard);
			Utils.printBoard(moveBoard);
			logger.info("computed move board " + computedMoveBoard);
			Utils.printBoard(computedMoveBoard);
		}
		assert (computedMoveBoard == moveBoard);
	}

	void parseFile(String filename) {
		logger.info("filename is " + filename);
		URL url = BoardGeneratorTest.class.getClassLoader().getResource(filename);

		List<String> allLines = new ArrayList<String>();
		try {
			Path path = Paths.get(url.toURI());
			allLines = Files.readAllLines(path);

		} catch (IOException e) {

			e.printStackTrace();

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		// three boards in one file
		Iterator<String> it = allLines.iterator();
		List<String> boardStrings = new ArrayList<String>();
		for (int r = 0; r < 8; ++r) {
			boardStrings.add(it.next());
		}
		pieceBoard = parseBoard(boardStrings);

		it.next();
		boardStrings.clear();
		for (int r = 0; r < 8; ++r) {
			boardStrings.add(it.next());
		}
		blockerBoard = parseBoard(boardStrings);

		it.next();
		boardStrings.clear();
		for (int r = 0; r < 8; ++r) {
			boardStrings.add(it.next());
		}
		firstBlockerBoard = parseBoard(boardStrings);

		it.next();
		boardStrings.clear();
		for (int r = 0; r < 8; ++r) {
			boardStrings.add(it.next());
		}
		moveBoard = parseBoard(boardStrings);

		// logger.info("piece");
		// BoardGenerator.printBoard(pieceBoard);
		// logger.info("blocker");
		// BoardGenerator.printBoard(blockerBoard);
		// logger.info("move");
		// BoardGenerator.printBoard(moveBoard);

	}

	// strings is a list of 8 strings,
	// one for each rank of the board
	long parseBoard(List<String> strings) {
		String bitString = "";
		for (String s : strings) {
			// reverse the string
			String reversed = "";
			for (char c : s.toCharArray()) {
				switch (c) {
				case '.':
					c = '0';
					break;
				case 'P':
					piece = PieceEnum.WPAWN;
					c = '1';
					break;
				case 'N':
					piece = PieceEnum.WNIGHT;
					c = '1';
					break;
				case 'B':
					piece = PieceEnum.WBISHOP;
					c = '1';
					break;
				case 'R':
					piece = PieceEnum.WROOK;
					c = '1';
					break;
				case 'Q':
					piece = PieceEnum.WQUEEN;
					c = '1';
					break;
				case 'K':
					piece = PieceEnum.WKING;
					c = '1';
					break;
				case 'p':
					piece = PieceEnum.BPAWN;
					c = '1';
					break;
				case 'n':
					piece = PieceEnum.BNIGHT;
					c = '1';
					break;
				case 'b':
					piece = PieceEnum.BBISHOP;
					c = '1';
					break;
				case 'r':
					piece = PieceEnum.BROOK;
					c = '1';
					break;
				case 'q':
					piece = PieceEnum.BQUEEN;
					c = '1';
					break;
				case 'k':
					piece = PieceEnum.BKING;
					c = '1';
					break;
				}
				reversed = c + reversed;
			}
			bitString += reversed;
		}
		long x = Long.parseLong(bitString, 2);
		return x;
	}
}
