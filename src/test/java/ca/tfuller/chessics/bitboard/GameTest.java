package ca.tfuller.chessics.bitboard;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GameTest {
	private static final Logger logger = LogManager.getLogger(GameTest.class);
	static Game game = new Game();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	// @Test
	void initialFenSetup() {
		String filename = "initial.txt";
		testOneFile(filename);
	}

	@Test
	void readFiles() {
		URL pathUrl = GameTest.class.getClassLoader().getResource("fen/");
		logger.info("path is " + pathUrl.getPath());
		if ((pathUrl != null) && pathUrl.getProtocol().equals("file")) {
			try {
				String[] pathnames = new File(pathUrl.toURI()).list();
				for (String s : pathnames) {
					logger.info(s);
					testOneFile(s);
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	void testOneFile(String s) {
		URL pathUrl = GameTest.class.getClassLoader().getResource(s);
		logger.info("url is " + pathUrl);

		try {
			File file = Paths.get(pathUrl.toURI()).toFile();
			game.loadGame(file);
			BoardOps.printBoard(game.getLastBitboards());
			String fenFromBitboard = FEN.toString(game.getLastBitboards());
			BoardOps.printState(game.getLastBitboards());
			logger.info("fen is ==>" + fenFromBitboard + "<==");
			String fenFromList = game.getFenList().peekLast();

			logger.info("last fen string from list:" + fenFromList);
			logger.info("fen from bitboard:        " + fenFromBitboard);

			assert (fenFromList.equals(fenFromBitboard));

		} catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
