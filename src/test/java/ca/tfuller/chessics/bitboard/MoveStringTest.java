package ca.tfuller.chessics.bitboard;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MoveStringTest {
	private static final Logger logger = LogManager.getLogger(MoveStringTest.class);
	static Game game = new Game();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() {
		testOneFile("chessics.txt");
	}

	void testOneFile(String s) {
		URL pathUrl = MoveStringTest.class.getClassLoader().getResource(s);
		logger.info("url is " + pathUrl);

		try {
			File file = Paths.get(pathUrl.toURI()).toFile();
			game.loadGame(file);

			String moves = game.getMoveList();
			logger.info("move list: " + System.lineSeparator() + moves);

		} catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
