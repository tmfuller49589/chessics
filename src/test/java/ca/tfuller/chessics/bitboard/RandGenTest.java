package ca.tfuller.chessics.bitboard;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RandGenTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() {
		int sumBits = 0;
		int maxBits = 0;
		int minBits = 64;
		int N = 10000000;
		for (int i = 0; i < N; ++i) {
			long r = BoardGenerator.randLong();
			// System.out.print(BoardGenerator.toBinaryString(r) + " " + r);
			// System.out.println();
			int bits = Long.bitCount(r);
			maxBits = Math.max(maxBits, bits);
			minBits = Math.min(minBits, bits);
			sumBits += Long.bitCount(r);
		}
		System.out.println("avg bit count is " + (sumBits / (float) N));
		System.out.println("min bit count is " + minBits);
		System.out.println("max bit count is " + maxBits);
	}

}
