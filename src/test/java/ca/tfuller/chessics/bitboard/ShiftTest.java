package ca.tfuller.chessics.bitboard;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ShiftTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() {
		long b = 0b0000000000000000000000000000000000000000000000000000000000000000l;
		long shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.E);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 0);

		b = 0b0000000000000000000000000000000000000000000000000000000000000001l;
		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.E);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 2);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.W);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 0);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.S);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 0);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.N);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 256);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.NE);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 512);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.SE);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 0);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.SW);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 0);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.NW);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 0);

		b = 0b0000000000000000000000000000000000100000000000000000000000000000l;
		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.NW);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == 68719476736l);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.N);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == b * 256);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.NE);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == b * 256 * 2);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.NW);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == b * 256 / 2);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.SE);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == b / 256 * 2);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.SW);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == b / 256 / 2);

		shifted = BoardGenerator.shiftBoardOne(b, DirectionEnum.SW);
		shifted = BoardGenerator.shiftBoardOne(shifted, DirectionEnum.SW);
		Utils.printBoard(b);
		Utils.printBoard(shifted);
		assert (shifted == b / 256 / 2 / 256 / 2);

	}

}
