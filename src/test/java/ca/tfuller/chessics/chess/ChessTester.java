package ca.tfuller.chessics.chess;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import ca.tfuller.chessics.chess.BoardOpsOld;

class ChessTester {

	private static final Logger logger = LogManager.getLogger();

	@Test
	void test() {

		logger.info("chess tester");

		ArrayList<String> fenList = new ArrayList<>();

		fenList.add("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0");
		fenList.add("rnbqkbnr/pppppppp/8/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq d3 0 0");
		fenList.add("rnbqkbnr/ppp1pppp/8/3p4/3P4/8/PPP1PPPP/RNBQKBNR b KQkq d6 0 1");
		fenList.add("rnbqkbnr/ppp1pppp/8/3p4/3P4/5N2/PPP1PPPP/RNBQKB1R w KQkq - 1 1");
		fenList.add("r1bqkbnr/ppp1pppp/2n5/3p4/3P4/5N2/PPP1PPPP/RNBQKB1R b KQkq - 2 2");
		fenList.add("r1bqkbnr/ppp1pppp/2n5/3p4/3P4/2P2N2/PP2PPPP/RNBQKB1R w KQkq - 0 2");
		fenList.add("r1bqkb1r/ppp1pppp/2n2n2/3p4/3P4/2P2N2/PP2PPPP/RNBQKB1R b KQkq - 1 3");
		fenList.add("r1bqkb1r/ppp1pppp/2n2n2/3p4/3P4/2P1PN2/PP3PPP/RNBQKB1R w KQkq - 0 3");
		fenList.add("r1bqkb1r/ppp2ppp/2n1pn2/3p4/3P4/2P1PN2/PP3PPP/RNBQKB1R b KQkq - 0 4");
		fenList.add("r1bqkb1r/ppp2ppp/2n1pn2/3p4/3P4/2PBPN2/PP3PPP/RNBQK2R w KQkq - 1 4");
		fenList.add("r1bqk2r/ppp2ppp/2nbpn2/3p4/3P4/2PBPN2/PP3PPP/RNBQK2R b KQkq - 2 5");
		fenList.add("r1bqk2r/ppp2ppp/2nbpn2/3p4/3P4/2PBPN2/PP3PPP/RNBQ2KR w kq - 3 5");
		fenList.add("r1bqk2r/ppp2ppp/2nbpn2/3p4/3P4/2PBPN2/PP3PPP/RNBQ1RK1 w kq - 4 5");
		fenList.add("r1bqk2r/p1p2ppp/2nbpn2/1p1p4/3P4/2PBPN2/PP3PPP/RNBQ1RK1 b kq b6 0 6");
		fenList.add("r1bqk2r/p1p2ppp/2nbpn2/1B1p4/3P4/2P1PN2/PP3PPP/RNBQ1RK1 w kq - 0 6");
		fenList.add("r2qk2r/p1pb1ppp/2nbpn2/1B1p4/3P4/2P1PN2/PP3PPP/RNBQ1RK1 b kq - 1 7");
		fenList.add("r2qk2r/p1pb1ppp/2nbpn2/1B1pN3/3P4/2P1P3/PP3PPP/RNBQ1RK1 w kq - 2 7");
		fenList.add("r2qk2r/p1pb1ppp/3bpn2/1B1pn3/3P4/2P1P3/PP3PPP/RNBQ1RK1 b kq - 0 8");
		fenList.add("r2qk2r/p1pb1ppp/3bpn2/1B1pP3/8/2P1P3/PP3PPP/RNBQ1RK1 w kq - 0 8");
		fenList.add("r2qk2r/p1pb1ppp/4pn2/1B1pb3/8/2P1P3/PP3PPP/RNBQ1RK1 b kq - 0 9");
		fenList.add("r2qk2r/p1pB1ppp/4pn2/3pb3/8/2P1P3/PP3PPP/RNBQ1RK1 w kq - 0 9");
		fenList.add("r3k2r/p1pq1ppp/4pn2/3pb3/8/2P1P3/PP3PPP/RNBQ1RK1 b kq - 0 10");
		fenList.add("r3k2r/p1pq1ppp/4pn2/3pb3/1P6/2P1P3/P4PPP/RNBQ1RK1 w kq b3 0 10");
		fenList.add("r3k2r/p2q1ppp/2p1pn2/3pb3/1P6/2P1P3/P4PPP/RNBQ1RK1 b kq - 0 11");
		fenList.add("r3k2r/p2q1ppp/2p1pn2/3pb3/QP6/2P1P3/P4PPP/RNB2RK1 w kq - 1 11");
		fenList.add("r3k2r/p2q1ppp/4pn2/2ppb3/QP6/2P1P3/P4PPP/RNB2RK1 b kq - 0 12");
		fenList.add("r3k2r/p2q1ppp/4pn2/2Ppb3/Q7/2P1P3/P4PPP/RNB2RK1 w kq - 0 12");
		fenList.add("r3k2r/p2q1ppp/4pn2/2P1b3/Q2p4/2P1P3/P4PPP/RNB2RK1 b kq - 0 13");
		fenList.add("r3k2r/p2q1ppp/4pn2/2P1b3/1Q1p4/2P1P3/P4PPP/RNB2RK1 w kq - 1 13");
		fenList.add("r3k2r/3q1ppp/p3pn2/2P1b3/1Q1p4/2P1P3/P4PPP/RNB2RK1 b kq - 0 14");
		fenList.add("r3k2r/3q1ppp/p3pn2/2P1b3/2Qp4/2P1P3/P4PPP/RNB2RK1 w kq - 1 14");
		fenList.add("r1k4r/3q1ppp/p3pn2/2P1b3/2Qp4/2P1P3/P4PPP/RNB2RK1 b - - 2 15");
		fenList.add("2kr3r/3q1ppp/p3pn2/2P1b3/2Qp4/2P1P3/P4PPP/RNB2RK1 b - - 3 16");

		String move = BoardOpsOld.getMove(fenList.get(0), fenList.get(1));

	}

}
