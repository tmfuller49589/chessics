package ca.tfuller.chessics.chess;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ca.tfuller.chessics.application.BoardController;

class EngineMiniMaxTest {
	static BoardOpsOld board = new BoardOpsOld();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() {
		Path path = Paths.get("test1.txt");

		BoardController bc = new BoardController();
		bc.loadGame(path.toFile());
	}

}
