package ca.tfuller.chessics.chess;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javafx.scene.image.Image;

class ResourceTest {

	protected static final Logger logger = LogManager.getLogger(ResourceTest.class);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testLogger() {
		logger.info("logger test");
	}

	@Test
	void test() {
		logger.info("logger test");

		Image dotImage = new Image(ResourceTest.class.getClassLoader().getResourceAsStream("spot.png"));

	}

}
